#ifndef TEMPEST_H
#define TEMPEST_H

#include <stddef.h>
#include <stdint.h>

#define TEMPEST__TYPE_MACRO_1(macro, name) \
    macro(R, name) \
    macro(I8, name) \
    macro(I16, name) \
    macro(I32, name)

#define TEMPEST__TYPE_MACRO_1_R(macro, name) \
    macro(R, name)

#define TEMPEST__TYPE_MACRO_2(macro, name) \
    macro(R_R, name) \
    macro(R_I8, name) \
    macro(R_I16, name) \
    macro(R_I32, name)

#define TEMPEST__TYPE_MACRO_2_INV(macro, name) \
    macro(R_R, name) \
    macro(I8_R, name) \
    macro(I16_R, name) \
    macro(I32_R, name)

#define TEMPEST__TYPE_MACRO(macro, type, inst)\
    TEMPEST__TYPE_MACRO_##type(macro, inst)

#define TEMPEST_MACRO_ALL_OPCODES(macro) \
    TEMPEST__TYPE_MACRO(macro, 2,     MOV) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   INC) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   DEC) \
    TEMPEST__TYPE_MACRO(macro, 2,     ADD) \
    TEMPEST__TYPE_MACRO(macro, 2,     SUB) \
    TEMPEST__TYPE_MACRO(macro, 2,     MUL) \
    TEMPEST__TYPE_MACRO(macro, 2,     DIV) \
    TEMPEST__TYPE_MACRO(macro, 2,     REM) \
    TEMPEST__TYPE_MACRO(macro, 2,     IADD) \
    TEMPEST__TYPE_MACRO(macro, 2,     ISUB) \
    TEMPEST__TYPE_MACRO(macro, 2,     IMUL) \
    TEMPEST__TYPE_MACRO(macro, 2,     IDIV) \
    TEMPEST__TYPE_MACRO(macro, 2,     IREM) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   NOT) \
    TEMPEST__TYPE_MACRO(macro, 2,     AND) \
    TEMPEST__TYPE_MACRO(macro, 2,     OR) \
    TEMPEST__TYPE_MACRO(macro, 2,     XOR) \
    TEMPEST__TYPE_MACRO(macro, 2,     SLL) \
    TEMPEST__TYPE_MACRO(macro, 2,     SRL) \
    TEMPEST__TYPE_MACRO(macro, 2,     SLA) \
    TEMPEST__TYPE_MACRO(macro, 2,     SRA) \
    TEMPEST__TYPE_MACRO(macro, 2,     EQ) \
    TEMPEST__TYPE_MACRO(macro, 2,     LT) \
    TEMPEST__TYPE_MACRO(macro, 2,     GT) \
    TEMPEST__TYPE_MACRO(macro, 2,     TEQ) \
    TEMPEST__TYPE_MACRO(macro, 2,     TLT) \
    TEMPEST__TYPE_MACRO(macro, 2,     TGT) \
    TEMPEST__TYPE_MACRO(macro, 1,     JMP) \
    TEMPEST__TYPE_MACRO(macro, 1,     JZ) \
    TEMPEST__TYPE_MACRO(macro, 1,     JNZ) \
    TEMPEST__TYPE_MACRO(macro, 1,     JC) \
    TEMPEST__TYPE_MACRO(macro, 1,     JNC) \
    TEMPEST__TYPE_MACRO(macro, 1,     JV) \
    TEMPEST__TYPE_MACRO(macro, 1,     JNV) \
    TEMPEST__TYPE_MACRO(macro, 2,     LB) \
    TEMPEST__TYPE_MACRO(macro, 2,     LH) \
    TEMPEST__TYPE_MACRO(macro, 2,     LW) \
    TEMPEST__TYPE_MACRO(macro, 2_INV, SB) \
    TEMPEST__TYPE_MACRO(macro, 2_INV, SH) \
    TEMPEST__TYPE_MACRO(macro, 2_INV, SW) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PSB) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PSH) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PSW) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PPB) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PPH) \
    TEMPEST__TYPE_MACRO(macro, 1_R,   PPW) \
    TEMPEST__TYPE_MACRO(macro, 1,     INT) \

#define TEMPEST_MACRO_ALL_REGISTERS(macro) \
    macro(0, R0) \
    macro(1, R1) \
    macro(2, R2) \
    macro(3, R3) \
    macro(4, R4) \
    macro(5, R5) \
    macro(6, R6) \
    macro(7, R7) \
    macro(8, R8) \
    macro(9, R9) \
    macro(10, R10) \
    macro(11, R11) \
    macro(12, R12) \
    macro(13, R13) \
    macro(14, R14) \
    macro(15, R15) \
    macro(16, SS) \
    macro(17, SP) \
    macro(18, SE) \
    macro(19, PC)

struct tempest
{
    uint32_t regs[20];
    uint8_t flags;

    uint8_t *data;
    uint32_t size;

    uint32_t intr;
    void* ext;
};

enum tempest_op
{
    TEMPEST_OP_NULL = 0,
    #define TEMPEST__OP_ENUM(addr, name) \
        TEMPEST_OP_##name##_##addr,
    TEMPEST_MACRO_ALL_OPCODES(TEMPEST__OP_ENUM)
};

enum tempest_reg
{
    #define TEMPEST__REG_ENUM(idx, name) \
        TEMPEST_REG_##name = idx,
    TEMPEST_MACRO_ALL_REGISTERS(TEMPEST__REG_ENUM)
};

enum tempest_flag
{
    TEMPEST_FLAG_Z = 1,
    TEMPEST_FLAG_C = 2,
    TEMPEST_FLAG_V = 4,
};

enum tempest_status
{
    TEMPEST_OK = 0,
    TEMPEST_HALT,
    TEMPEST_ESEGV,
    TEMPEST_ESTOV,
    TEMPEST_ESTUN,
    TEMPEST_EUNSUP
};

enum tempest_status tempest_next(struct tempest *cpu);
enum tempest_status tempest_run(struct tempest *cpu);

#endif
