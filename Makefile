CFLAGS += -O2 -std=c99 -Wall -Wextra -Wpedantic
CFLAGS += -Wno-unused-function

INDEXPKG = -Ideps/Index/include

.PHONY: all install uninstall clean
all: build/libtempest.a build/libtempest.so build/tempest-as
install:
	cp -r 'include/tempest'   "$(DESTDIR)/usr/include/"
	cp 'build/libtempest.a'   "$(DESTDIR)/usr/lib/"
	cp 'build/libtempest.so'  "$(DESTDIR)/usr/lib/"
	cp 'build/tempest-as'     "$(DESTDIR)/usr/bin/"
	cp -r 'man/'*             "$(DESTDIR)/usr/share/man/"
uninstall:
	rm -rf "$(DESTDIR)/usr/include/tempest"
	rm -f  "$(DESTDIR)/usr/lib/libtempest.a"
	rm -f  "$(DESTDIR)/usr/lib/libtempest.so"
	rm -f  "$(DESTDIR)/usr/bin/tempest-as"
	rm -f  "$(DESTDIR)/usr/share/man/man1/tempest-as.1"
	rm -f  "$(DESTDIR)/usr/share/man/man3/tempest_next.3"
	rm -f  "$(DESTDIR)/usr/share/man/man3/tempest_run.3"
	rm -f  "$(DESTDIR)/usr/share/man/man3/tempest_types.3"
	rm -f  "$(DESTDIR)/usr/share/man/man7/tempest.7"
clean:
	rm -rf build
depclean:
	rm -rf deps


build/libtempest.o: src/tempest/core.c include/tempest/core.h
	mkdir -p build
	$(CC) -c $(CFLAGS) -Iinclude 'src/tempest/'* -o $@

build/libtempest.a: build/libtempest.o 
	ar ruv $@ $<
	ranlib $@

build/libtempest.so: build/libtempest.o
	$(CC) $(CFLAGS) -shared -o $@ $<

build/tempest-as: build/libtempest.o src/assembler.c deps/Index/include
	$(CC) $(CFLAGS) $(INDEXPKG) -Iinclude $< src/assembler.c -o $@


deps:
	mkdir deps

deps/Index/include: | deps
	cd deps && git clone https://gitlab.com/BasedRimuru/Index
