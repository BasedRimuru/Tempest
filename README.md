# Tempest Project 

Tempest is a virtual CPU architecture made to control
other programs via programmable input (aka scripts).
It's designed to be simple and small, as an alternative to
scripting languages.

The reference implementation, libtempest, can be compiled to any
little-endian machine that supports C99, it can also be used in
freestanding environments.

It includes a simple assembler, tempest-as, which depends only on
the C99 libc and on libtempest.h.

The architecture specification and manuals are written as manpages.

## Development state
The architecture is frozen and libtempest is stable, no new additions
will be made other than a possible support for emulation in other endianess,
and bugfixes.

The assembler is now on beta. Hardly the syntax will be changed again, but
there may still be some bugs around.

More resources will be written in the future, including tutorials.

## Syntax example
```assembly
header:
    .assert . 0

    .string 0 "MY_ABI"
    .field 1 0
    .field 4 data
    .field 4 entry
    .align 32

    .assert . 32

data:
    bg:      .embed bg.rgb      $ bg_end:
    sprite:  .embed sprite.rgba $ sprite_end:

    .define bg_w     320 $ .define bg_h     240
    .define sprite_w  30 $ .define sprite_h  30

    .assert [bg_end bg -]         [bg_w bg_h 3 * *]
    .assert [sprite_end sprite -] [sprite_w sprite_h 4 * *]

.define set_bg 0x4
.define set_sprite 0x10
.define do_something 0x0A
entry:
    mov r0 bg     $ mov r1 bg_w     $ mov r2 bg_h     $ int set_bg
    mov r0 sprite $ mov r1 sprite_w $ mov r2 sprite_h $ int set_sprite
    int do_something
```
