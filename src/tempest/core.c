#include "tempest/core.h"

/* Aliases */
/* Only for this object, otherwise it could interfere with user types */

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;

#define REG(n) cpu->regs[n]
#define REG_SS REG(16)
#define REG_SP REG(17)
#define REG_SE REG(18)
#define REG_PC REG(19)

/* Safety checks */
/* 0 means success, 1 means an error */

#define CHECK_BORDERS(addr) ((addr) >= cpu->size)
#define CHECK_STACK_PUSH(size) \
    (((signed)REG_SP - (signed)(size)) < (signed)REG_SS)
#define CHECK_STACK_POP(size) \
    ((REG_SP + (size)) > REG_SE)

/* Read/Store definitions */
/* These macros controls how memory is accessed */

#define READ8(addr)     cpu->data[addr]
#define STORE8(addr, x) cpu->data[addr] = x
#define READ16(addr) \
    (union {u8 in[2]; u16 out;})\
    {.in[0] = cpu->data[addr + 0], \
     .in[1] = cpu->data[addr + 1]}.out
#define READ32(addr) \
    (union {u8 in[4]; u32 out;})\
    {.in[0] = cpu->data[addr + 0], \
     .in[1] = cpu->data[addr + 1], \
     .in[2] = cpu->data[addr + 2], \
     .in[3] = cpu->data[addr + 3]}.out
#define STORE16(addr, x) \
    cpu->data[addr + 0] = (union {u16 in; u8 out[2];}){.in = x}.out[0]; \
    cpu->data[addr + 1] = (union {u16 in; u8 out[2];}){.in = x}.out[1]
#define STORE32(addr, x) \
    cpu->data[addr + 0] = (union {u32 in; u8 out[4];}){.in = x}.out[0]; \
    cpu->data[addr + 1] = (union {u32 in; u8 out[4];}){.in = x}.out[1]; \
    cpu->data[addr + 2] = (union {u32 in; u8 out[4];}){.in = x}.out[2]; \
    cpu->data[addr + 3] = (union {u32 in; u8 out[4];}){.in = x}.out[3]

/* Clock definitions */
/* These macros read the memory, while advancing the program counter
 * and doing the proper tests */
#define CLOCK8(x) \
    if (CHECK_BORDERS(REG_PC)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else \
    { \
        x = READ8(REG_PC); \
        (REG_PC)++; \
    } 

#define CLOCK16(x) \
    if (CHECK_BORDERS(REG_PC + 1)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else \
    { \
        x = READ16(REG_PC); \
        (REG_PC) += 2; \
    } 

#define CLOCK32(x) \
    if (CHECK_BORDERS(REG_PC + 3)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else \
    { \
        x = READ32(REG_PC); \
        (REG_PC) += 4; \
    } 

/* Helper macros */
/* These macros simplifies some operations */

#define SIGN(bits, a) \
    (((a) & ((u##bits) 1 << (bits - 1))) != 0)

#define FLAG_GET(name) \
    (cpu->flags & TEMPEST_FLAG_##name)

#define FLAG_SET(name, val) \
    if ((val) != 0) \
    { \
        cpu->flags |= TEMPEST_FLAG_##name; \
    } \
    else \
    { \
        cpu->flags &= ~(TEMPEST_FLAG_##name); \
    } \

#define COMMON_MATH(bits, a, exp, carry, overflow) \
    u##bits result = exp; \
    FLAG_SET(Z, (result == 0))  \
    FLAG_SET(C, carry) \
    FLAG_SET(V, overflow) \
    (a) = result; \

#define COMMON_LOAD(bits, a, bits2, b, bits3) \
    u##bits2 start = (b); \
    u##bits2 end = start + sizeof(u##bits3); \
    (void) start, (void) end; \
\
    if (CHECK_BORDERS(end)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else \
    { \
        (a) = READ##bits3(start); \
    }

#define COMMON_STORE(bits, a, bits2, b, bits3) \
    u##bits start = (a); \
    u##bits end = start + sizeof(u##bits3); \
    (void) start, (void) end; \
\
    if (CHECK_BORDERS(end)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else \
    { \
        STORE##bits3(start, b); \
    }

#define COMMON_PUSH(bits, a, bits2) \
    u32 size = sizeof(u##bits2); \
\
    if (CHECK_BORDERS(REG_SP)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else if (CHECK_STACK_PUSH(size)) \
    { \
        ret = TEMPEST_ESTOV; \
    } \
    else \
    { \
        REG_SP -= size; \
        STORE##bits2(REG_SP, a); \
    }

#define COMMON_POP(bits, a, bits2) \
    u32 size = sizeof(u##bits2); \
\
    if (CHECK_BORDERS(REG_SP + size - 1)) \
    { \
        ret = TEMPEST_ESEGV; \
    } \
    else if (CHECK_STACK_POP(size)) \
    { \
        ret = TEMPEST_ESTUN; \
    } \
    else \
    { \
        (a) = READ##bits2(REG_SP); \
        REG_SP += size; \
    }

/* Instruction macros */
/* These macros will be used to create the functions */

#define MOV_MACRO(bits, a, bits2, b) \
    (a) = (b); \

#define INC_MACRO(bits, a) \
    (a)++;
                       
#define DEC_MACRO(bits, a) \
    (a)--;

#define ADD_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) + (b), result < (a), 0)

#define SUB_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) - (b), result > (a), 0)

#define MUL_MACRO(bits, a, bits2, b) \
    if (a == 0 || b == 0) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 0) FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else \
    { \
        COMMON_MATH(bits, a, (a) * (b), (result / (b)) != (a), 0) \
    }

#define DIV_MACRO(bits, a, bits2, b) \
    if ((b) == 0) \
    { \
        FLAG_SET(Z, 0) FLAG_SET(C, 1) FLAG_SET(V, 0) \
        (a) = ((u##bits) -1); \
    } \
    else \
    { \
        COMMON_MATH(bits, a, (a) / (b), 0, 0) \
    } \

#define REM_MACRO(bits, a, bits2, b) \
    if ((b) == 0) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 1) FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else \
    { \
        COMMON_MATH(bits, a, (a) % (b), 0, 0) \
    } \

#define IADD_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) + (b), result < (a), \
                       (((i##bits) result) < ((i##bits) a)))

#define ISUB_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) - (b), result > (a), \
                       (((i##bits) result) > ((i##bits) (a))))

#define IMUL_MACRO(bits, a, bits2, b) \
    if (a == 0 || b == 0) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 0) FLAG_SET(V, SIGN(bits, a)) \
        (a) = 0; \
    } \
    else \
    { \
        COMMON_MATH(bits, a, (a) * (b), (result / (b)) != (a), \
                           ((SIGN(bits, a) ^ SIGN(bits2, b)) \
                           != SIGN(bits, result))) \
    }

#define IDIV_MACRO(bits, a, bits2, b) \
    i##bits smin = ((u##bits) 1 << (bits - 1)); \
\
    i##bits sa = *((i##bits*)&(a)); \
    i##bits2 sb = *((i##bits2*)&(b)); \
\
    if (sb == 0) \
    { \
        FLAG_SET(Z, 0) FLAG_SET(C, 1) FLAG_SET(V, SIGN(bits, a) != 1) \
        (a) = ((u##bits) -1); \
    } \
    else if ((sa == smin) && (sb == -1)) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 1) FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else \
    { \
        COMMON_MATH(bits, a, ((sa) / (sb)), 0, 0) \
    }

#define IREM_MACRO(bits, a, bits2, b) \
    i##bits smin = ((u##bits) 1 << (bits - 1)); \
\
    i##bits sa = *((i##bits*)&(a)); \
    i##bits2 sb = *((i##bits2*)&(b)); \
\
    if (sb == 0) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 1) FLAG_SET(V, SIGN(bits, a)) \
        (a) = 0; \
    } \
    else if ((sa == smin) && (sb == -1)) \
    { \
        FLAG_SET(Z, 1) FLAG_SET(C, 1) FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else \
    { \
        u##bits2 ub = ((sb) < 0) ? (-(b)) : (b); \
        if (sa >= 0) \
        { \
            COMMON_MATH(bits, a, (a) % (ub), 0, 0) \
        } \
        else \
        { \
            u##bits2 ua = ((sa) < 0) ? (-(a)) : (a); \
            COMMON_MATH(bits, a, -((ua) % (ub)), 0, 0) \
        } \
    }

#define NOT_MACRO(bits, a) \
    COMMON_MATH(bits, a, ~(a), 0, 0)

#define AND_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) & (b), 0, 0)

#define OR_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) | (b), 0, 0)

#define XOR_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) ^ (b), 0, 0)

#define SLL_MACRO(bits, a, bits2, b) \
    u8 shift = (b); \
    if (shift >= bits) \
    { \
        FLAG_SET(Z, 1) \
        FLAG_SET(C, SIGN(bits, a << (shift - 1))) \
        FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else if (shift != 0) \
    { \
        u8 carry = SIGN(bits, a << (shift - 1)); \
        COMMON_MATH(bits, a, (a) <<= (shift), carry, 0) \
    } \
    else \
    { \
        FLAG_SET(Z, a == 0) FLAG_SET(C, 0) FLAG_SET(V, 0) \
    }

#define SRL_MACRO(bits, a, bits2, b) \
    u8 shift = (b); \
    if (shift >= bits) \
    { \
        FLAG_SET(Z, 1) \
        FLAG_SET(C, ((a) >> (shift - 1)) & 1) \
        FLAG_SET(V, 0) \
        (a) = 0; \
    } \
    else if (shift != 0) \
    { \
        u8 carry = ((a) >> (shift - 1)) & 1; \
        COMMON_MATH(bits, a, (a) >>= (shift), carry, 0) \
    } \
    else \
    { \
        FLAG_SET(Z, a == 0) FLAG_SET(C, 0) FLAG_SET(V, 0) \
    }

#define SLA_MACRO(bits, a, bits2, b) \
    SLL_MACRO(bits, a, bits2, b)

#define SRA_MACRO(bits, a, bits2, b) \
    u8 shift = (b); \
    if (shift >= bits) \
    { \
        (a) = (SIGN(bits, a) == 1) ? 0xffffffff : 0; \
        FLAG_SET(Z, 1) \
        FLAG_SET(C, ((a) >> (shift - 1)) & 1) \
        FLAG_SET(V, 0) \
    } \
    else if (shift != 0) \
    { \
        u8 carry = ((a) >> (shift - 1)) & 1; \
        u##bits mask = 0; \
        if (SIGN(bits, a) == 1) \
            mask = (u##bits)0xffffffff << (bits - shift); \
        (a) >>= shift; \
        COMMON_MATH(bits, a, (a) |= mask, carry, 0) \
    } \
    else \
    { \
        FLAG_SET(Z, a == 0) FLAG_SET(C, 0) FLAG_SET(V, 0) \
    }

#define EQ_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) == (b), 0, 0)

#define LT_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) < (b), 0, 0)

#define GT_MACRO(bits, a, bits2, b) \
    COMMON_MATH(bits, a, (a) > (b), 0, 0)

#define TEQ_MACRO(bits, a, bits2, b) \
    u8 null = 0; \
    COMMON_MATH(bits, null, (a) == (b), 0, 0) \
    (void) null;

#define TLT_MACRO(bits, a, bits2, b) \
    u8 null = 0; \
    COMMON_MATH(bits, null, (a) < (b), 0, 0) \
    (void) null;

#define TGT_MACRO(bits, a, bits2, b) \
    u8 null = 0; \
    COMMON_MATH(bits, null, (a) > (b), 0, 0) \
    (void) null;

#define JMP_MACRO(bits, a) \
    REG_PC = (a);

#define JZ_MACRO(bits, a) \
    if (FLAG_GET(Z) != 0) JMP_MACRO(bits, a);

#define JNZ_MACRO(bits, a) \
    if (FLAG_GET(Z) == 0) JMP_MACRO(bits, a);

#define JC_MACRO(bits, a) \
    if (FLAG_GET(C) != 0) JMP_MACRO(bits, a);

#define JNC_MACRO(bits, a) \
    if (FLAG_GET(C) == 0) JMP_MACRO(bits, a);

#define JV_MACRO(bits, a) \
    if (FLAG_GET(V) != 0) JMP_MACRO(bits, a);

#define JNV_MACRO(bits, a) \
    if (FLAG_GET(V) == 0) JMP_MACRO(bits, a);

#define LB_MACRO(bits, a, bits2, b) \
    COMMON_LOAD(bits, a, bits2, b, 8)

#define LH_MACRO(bits, a, bits2, b) \
    COMMON_LOAD(bits, a, bits2, b, 16)

#define LW_MACRO(bits, a, bits2, b) \
    COMMON_LOAD(bits, a, bits2, b, 32)

#define SB_MACRO(bits, a, bits2, b) \
    COMMON_STORE(bits, a, bits2, b, 8)

#define SH_MACRO(bits, a, bits2, b) \
    COMMON_STORE(bits, a, bits2, b, 16)

#define SW_MACRO(bits, a, bits2, b) \
    COMMON_STORE(bits, a, bits2, b, 32)

#define PSB_MACRO(bits, a) \
    COMMON_PUSH(bits, a, 8)

#define PSH_MACRO(bits, a) \
    COMMON_PUSH(bits, a, 16)

#define PSW_MACRO(bits, a) \
    COMMON_PUSH(bits, a, 32)

#define PPB_MACRO(bits, a) \
    COMMON_POP(bits, a, 8)

#define PPH_MACRO(bits, a) \
    COMMON_POP(bits, a, 16)

#define PPW_MACRO(bits, a) \
    COMMON_POP(bits, a, 32)

#define INT_MACRO(bits, a) \
    cpu->intr = a; \
    ret = TEMPEST_HALT;

/* Function macros */
/* These macros will use the instruction macros to define the functions 
 * for each instruction */

#define DEF_OP1_FUNC(name, bits, test, statement) \
static inline enum tempest_status \
name(struct tempest *cpu) \
{ \
    enum tempest_status ret = TEMPEST_OK; \
\
    u##bits a = 0; \
\
    CLOCK##bits(a); \
    if (ret == 0) \
        ret = test(a); \
\
    if (ret == 0) \
    { \
        statement \
    } \
\
    return ret; \
}

#define DEF_OP2_FUNC(name, bits, bits2, test, test2, statement) \
static inline enum tempest_status \
name(struct tempest *cpu) \
{ \
    enum tempest_status ret = TEMPEST_OK; \
\
    u##bits a; \
    u##bits2 b; \
\
    CLOCK##bits(a) \
    if (ret == 0) \
    { \
        CLOCK##bits2(b) \
    } \
    if (ret == 0) \
        ret = test(a); \
    if (ret == 0) \
        ret = test2(b); \
\
    if (ret == 0) \
    { \
        statement \
    } \
\
    return ret; \
}

#define TEST_REGISTER(id) (id > 20) ? TEMPEST_EUNSUP : TEMPEST_OK
#define TEST_NOTHING(id) TEMPEST_OK
    
#define DEF_OP_R(name, macro) \
    DEF_OP1_FUNC(name##_R, 8, TEST_REGISTER, macro(32, REG(a)))

#define DEF_OP_I8(name, macro) \
    DEF_OP1_FUNC(name##_I8, 8, TEST_NOTHING, macro(8, a))

#define DEF_OP_I16(name, macro) \
    DEF_OP1_FUNC(name##_I16, 16, TEST_NOTHING, macro(16, a))

#define DEF_OP_I32(name, macro) \
    DEF_OP1_FUNC(name##_I32, 32, TEST_NOTHING, macro(32, a))

#define DEF_OP_R_R(name, macro) \
    DEF_OP2_FUNC(name##_R_R, 8, 8, TEST_REGISTER, TEST_REGISTER, \
                 macro(32, REG(a), 32, REG(b)))

#define DEF_OP_R_I8(name, macro) \
    DEF_OP2_FUNC(name##_R_I8, 8, 8, TEST_REGISTER, TEST_NOTHING, \
                 macro(32, REG(a), 8, b))

#define DEF_OP_R_I16(name, macro) \
    DEF_OP2_FUNC(name##_R_I16, 8, 16, TEST_REGISTER, TEST_NOTHING, \
                 macro(32, REG(a), 16, b))

#define DEF_OP_R_I32(name, macro) \
    DEF_OP2_FUNC(name##_R_I32, 8, 32, TEST_REGISTER, TEST_NOTHING, \
                 macro(32, REG(a), 32, b))

#define DEF_OP_I8_R(name, macro) \
    DEF_OP2_FUNC(name##_I8_R, 8, 8, TEST_NOTHING, TEST_REGISTER, \
                 macro(8, a, 32, REG(b)))

#define DEF_OP_I16_R(name, macro) \
    DEF_OP2_FUNC(name##_I16_R, 16, 8, TEST_NOTHING, TEST_REGISTER, \
                 macro(16, a, 32, REG(b)))

#define DEF_OP_I32_R(name, macro) \
    DEF_OP2_FUNC(name##_I32_R, 32, 8, TEST_NOTHING, TEST_REGISTER, \
                 macro(32, a, 32, REG(b)))

#define DEF_OP(type, id) \
    DEF_OP##_##type(op##_##id, id##_MACRO)

TEMPEST_MACRO_ALL_OPCODES(DEF_OP)

/* Parsing functions */
/* These functions are the ones that are exported to be used */

#define PARSE_CASE(addr, inst) \
    case TEMPEST_OP##_##inst##_##addr: \
        ret = op##_##inst##_##addr(cpu); \
        break;

#define PARSE_OP(inst) \
    CLOCK8(inst) \
    if (ret == TEMPEST_OK) \
    { \
        switch (inst) \
        { \
            TEMPEST_MACRO_ALL_OPCODES(PARSE_CASE) \
            default: \
                ret = TEMPEST_EUNSUP; \
                break; \
        } \
    }

extern enum tempest_status
tempest_next(struct tempest *cpu)
{
    enum tempest_status ret = TEMPEST_OK;

    u8 inst = 0;
    PARSE_OP(inst)
    
    return ret;
}

extern enum tempest_status
tempest_run(struct tempest *cpu)
{
    enum tempest_status ret = TEMPEST_OK;
  
    while (ret == TEMPEST_OK)
    {
        u8 inst = 0;
        PARSE_OP(inst)
    }

    return ret;
}
