#include <ctype.h> /* toupper, tolower, isspace, isprint, isalnum */
#include <stdio.h> /* FILE, stderr, fprintf, fopen, fclose, rewind,
                      fwrite, getc, EOF */
#include <stdint.h> /* uint8_t, uint16_t, uint32_t */
#include <stdlib.h> /* malloc, free, strtol */
#include <string.h> /* strlen, strcmp, strcpy */
#include <stdarg.h> /* va_list, va_start, va_end */
#include <stdbool.h> /* bool, true, false */

#include "tempest/core.h" /* TEMPEST_MACRO_ALL_OPCODES,
                             TEMPEST_MACRO_ALL_REGISTERS */

/* String definitions */
/* Defines the strings on the correct language */

#define EN_US 1
#define PT_BR 2

#ifndef LANG
#define LANG EN_US
#endif

#if LANG == PT_BR
    #define MSG_USAGE     "uso: tempest-as entrada [saída] [encadeamento]"
    #define MSG_FTL       "fatal:"
    #define MSG_ERR       "erro:"
    #define MSG_WARN      "aviso:"
    #define MSG_DBG       "depuração:"
    #define DBG_REG       "registrador"
    #define DBG_ADDR      "endereço"
    #define DBG_CONS      "constante"
    #define DBG_LINK      "encadeável"
    #define DBG_INV       "inválido"
    #define ERR_INIT      "falhei ao inicializar"
    #define ERR_EXPF      "falhei ao executar sua expressão"
    #define ERR_EXPL      "expressão é longa demais"
    #define ERR_EXPT      "expressão criou resultados demais"
    #define ERR_EXPZ      "expressão não deu resultados"
    #define ERR_OREAD     "falhei ao abrir arquivo para leitura"
    #define ERR_OWRIT     "falhei ao abrir arquivo para escrita"
    #define ERR_FSIZE     "arquivo possui tamanho indeterminável"
    #define ERR_FSAME     "entrada e saída são o mesmo arquivo"
    #define ERR_SEEKB     "falhei ao rebobinar arquivo"
    #define ERR_NOOUT     "falhei ao escrever para saída"
    #define ERR_NOMEM     "falhei ao alocar memória"
    #define ERR_LROOT     "tentando ir acima do contexto raíz"
    #define ERR_ARGSM     "poucos argumentos"
    #define ERR_ARGSE     "argumentos demais"
    #define ERR_ASSERT    "asserção falhou"
    #define ERR_UNKDIR    "diretiva desconhecida"
    #define ERR_UNKPIN    "pseudo-instrução desconhecida"
    #define ERR_UNKINS    "instrução desconhecida"
    #define ERR_UNKSYM    "símbolo desconhecido"
    #define ERR_UNKADDR   "endereçamento desconhecido"
    #define ERR_UNKMACRO  "macro desconhecido"
    #define ERR_BADTOK    "componente léxico não terminado"
    #define ERR_BADSYM    "símbolo inválido"
    #define ERR_BADOPR    "operador inválido"
    #define ERR_BADESC    "caractere de escape inválido"
    #define ERR_BADNUM    "número inválido"
    #define ERR_BADARG    "argumento inválido"
    #define ERR_BADCHAR   "caractere inválido"
    #define ERR_BADCHAR2  "constante de caractere inválida"
    #define ERR_BADSTR    "sequência de caracteres inválida"
    #define ERR_BADLINK   "sem dados de encadeamento"
    #define ERR_MISSIF    "faltando um %if"
    #define ERR_MISSFI    "faltando um %fi"
    #define ERR_MISSSTA   "faltando um %macro"
    #define ERR_MISSEND   "faltando um %endmacro"
#elif LANG == EN_US
    #define MSG_USAGE     "usage: tempest-as input [output] [linking]"
    #define MSG_FTL       "fatal:"
    #define MSG_ERR       "error:"
    #define MSG_WARN      "warning:"
    #define MSG_DBG       "debug:"
    #define DBG_REG       "register"
    #define DBG_ADDR      "address"
    #define DBG_CONS      "constant"
    #define DBG_LINK      "linkable"
    #define DBG_INV       "invalid"
    #define ERR_INIT      "failed to initialize"
    #define ERR_EXPF      "failed to run your expression"
    #define ERR_EXPL      "expression is too long"
    #define ERR_EXPT      "expression yields too many results"
    #define ERR_EXPZ      "expression yields no results"
    #define ERR_OREAD     "failed to open your file for reading"
    #define ERR_OWRIT     "failed to open your file for writing"
    #define ERR_FSIZE     "embedded file has indeterminable size"
    #define ERR_FSAME     "input and output files are the same"
    #define ERR_SEEKB     "failed to seek back"
    #define ERR_NOOUT     "failed to write output"
    #define ERR_NOMEM     "failed to allocate memory"
    #define ERR_LROOT     "trying to go above the root context"
    #define ERR_ARGSM     "too few args"
    #define ERR_ARGSE     "too many args"
    #define ERR_ASSERT    "assertion failed"
    #define ERR_UNKDIR    "unknown directive"
    #define ERR_UNKPIN    "unknown pseudo-instruction"
    #define ERR_UNKINS    "unknown instruction"
    #define ERR_UNKSYM    "unknown symbol"
    #define ERR_UNKADDR   "unknown addressing"
    #define ERR_UNKMACRO  "unknown macro"
    #define ERR_BADTOK    "unfinished token"
    #define ERR_BADSYM    "invalid symbol"
    #define ERR_BADOPR    "bad operand"
    #define ERR_BADESC    "bad escape character"
    #define ERR_BADNUM    "invalid number"
    #define ERR_BADARG    "invalid argument"
    #define ERR_BADCHAR   "invalid char"
    #define ERR_BADCHAR2  "bad char constant"
    #define ERR_BADSTR    "bad string"
    #define ERR_BADLINK   "no linking data"
    #define ERR_MISSIF    "missing %if"
    #define ERR_MISSFI    "missing %fi"
    #define ERR_MISSSTA   "missing %macro"
    #define ERR_MISSEND   "missing %endmacro"
#else
    #define MSG_USAGE     ""
    #define MSG_FTL       ""
    #define MSG_ERR       ""
    #define MSG_WARN      ""
    #define MSG_DBG       ""
    #define DBG_REG       ""
    #define DBG_ADDR      ""
    #define DBG_CONS      ""
    #define DBG_LINK      ""
    #define DBG_INV       ""
    #define ERR_INIT      ""
    #define ERR_EXPL      ""
    #define ERR_EXPF      ""
    #define ERR_EXPT      ""
    #define ERR_EXPZ      ""
    #define ERR_OREAD     ""
    #define ERR_OWRIT     ""
    #define ERR_FSIZE     ""
    #define ERR_FSAME     ""
    #define ERR_SEEKB     ""
    #define ERR_NOOUT     ""
    #define ERR_NOMEM     ""
    #define ERR_LROOT     ""
    #define ERR_ARGSM     ""
    #define ERR_ARGSE     ""
    #define ERR_ASSERT    ""
    #define ERR_BADTOK    ""
    #define ERR_UNKDIR    ""
    #define ERR_UNKPIN    ""
    #define ERR_UNKINS    ""
    #define ERR_UNKSYM    ""
    #define ERR_BADSYM    ""
    #define ERR_BADOPR    ""
    #define ERR_BADESC    ""
    #define ERR_BADNUM    ""
    #define ERR_BADSTR    ""
    #define ERR_UNKADDR   ""
    #define ERR_UNKMACRO  ""
    #define ERR_BADARG    ""
    #define ERR_BADCHAR   ""
    #define ERR_BADCHAR2  ""
    #define ERR_BADLINK   ""
    #define ERR_MISSIF    ""
    #define ERR_MISSFI    ""
    #define ERR_MISSSTA   ""
    #define ERR_MISSEND   ""
    #error Unknown language
#endif

/* Type definitions */
/* Defines the necessary types for this file functions */

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;

enum as_status
{
    AS_OK,
    AS_ERROR,
    AS_FATAL
};

enum sym_type
{
    SYM_NULL,
    SYM_INST,
    SYM_REG,
    SYM_ADDR,
    SYM_DUMMY,
    SYM_CONST,
    SYM_LINK
};

/* External functions */
/* Needs to be included before struct assembler definition */

struct sym
{
    enum sym_type type;
    char *string;
    u32 value;
};

static size_t
symbol_map_hash(struct sym *sy)
{
    size_t ret = 5381;

    if (sy != NULL && sy->string != NULL)
    {
        for (char *string = sy->string; *string != '\0'; string = &(string[1]))
            ret = ((ret << 5) + ret) + string[0];
    }

    return ret;
}

static bool
symbol_map_test(struct sym *sy, struct sym *sy2)
{
    bool ret = false;

    if (sy != NULL && sy2 != NULL && sy->string != NULL && sy2->string != NULL)
    {
        if (strcmp(sy->string, sy2->string) == 0)
            ret = true;
    }

    return ret;
}

static void
symbol_map_cleanup(struct sym *sy)
{
    if (sy != NULL)
        free(sy->string);
}

#define X_TYPE struct sym
#define X_NAMING(x) symbol_map_##x
#define X_CLEANUP
#include "structure/mapping.def"
#undef X_TYPE
#undef X_NAMING
#undef X_CLEANUP

#define X_TYPE char
#define X_NAMING(x) char_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

#define X_TYPE char*
#define X_NAMING(x) charptr_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

struct macro
{
    char *name;
    struct charptr_vec_t *values;
};

static size_t
macro_map_hash(struct macro *m)
{
    size_t ret = 5381;

    if (m != NULL && m->name != NULL)
    {
        for (char *name = m->name; *name != '\0'; name = &(name[1]))
            ret = ((ret << 5) + ret) + name[0];
    }

    return ret;
}

static bool
macro_map_test(struct macro *m, struct macro *m2)
{
    bool ret = false;

    if (m != NULL && m2 != NULL && m->name != NULL && m2->name != NULL)
    {
        if (strcmp(m->name, m2->name) == 0)
            ret = true;
    }

    return ret;
}

static void
macro_map_cleanup(struct macro *m)
{
    if (m != NULL)
    {
        free(m->name);
        for (u32 i = 0; i < m->values->count; i++)
            free(m->values->data[i]);
        charptr_vec_free(m->values);
    }
}

#define X_TYPE struct macro
#define X_NAMING(x) macro_map_##x
#define X_CLEANUP
#include "structure/mapping.def"
#undef X_TYPE
#undef X_NAMING
#undef X_CLEANUP

/* Struct assembler definition */
/* Main type for assembler functions */

struct assembler
{
    enum as_status status;
    u32 line_err;

    FILE *input;
    char *line;
    u32 line_n;
    u32 line_c;
    u8 line_t;
    char *line_i;

    u32 counter;
    u8 pass;

    struct symbol_map_t *symbols;
    char *context;

    char *filename;
    FILE *output;

    FILE *linking;

    bool cond_ip;
    bool cond_state;

    struct charptr_vec_t *extra_lines;
    struct charptr_vec_t *lines_info;
    struct charptr_vec_t *extra_tokens;

    struct macro_map_t *macros;
    char *macro_args[10];
    bool in_macro;

    struct macro_map_t *words;
};

/* Symbol functions */
/* Creates and destroy symbols */
/* Symbols outside a symbol table needs to be manually deleted */

static struct sym *
sym_new(enum sym_type type, char *string, u32 value)
{
    struct sym *ret = (struct sym *) malloc(sizeof(struct sym));

    if (ret != NULL)
    {
        ret->type = type;
        ret->value = value;

        u32 length = strlen(string);
        ret->string = (char *) malloc(length + 1);
        if (ret->string != NULL)
        {
            strcpy(ret->string, string);
        }
        else
        {
            free(ret);
            ret = NULL;
        }
    }

    return ret;
}

static struct sym *
sym_del(struct sym *sy)
{
    if (sy != NULL)
        free(sy->string);
    free(sy);
    return NULL;
}

/* Instruction adressing */
/* Takes care of the suffixes for the correct opcodes */

enum addressing
{
    ADDR_NULL = 0, ADDR_REG, ADDR_I8, ADDR_I16, ADDR_I32, ADDR_SYM, ADDR_LINK
};

const char* addr_suffixes[7] = {"", "_r", "_i8", "_i16",
                                "_i32", "_i32", "_i32"};

static enum addressing
addr_type(struct assembler *as, struct sym *sy)
{
    u8 ret = ADDR_NULL;

    switch (sy->type)
    {
        case SYM_INST:
            ret = ADDR_I8;
            break;
        case SYM_REG:
            ret = ADDR_REG;
            break;
        case SYM_LINK:
        case SYM_DUMMY:
            ret = ADDR_LINK;
            break;
        case SYM_ADDR:
            if (as->linking != NULL)
            {
                ret = ADDR_SYM;
            }
            else
            {
                if (sy->value >= 65536 || sy->value > as->counter)
                {
                    ret = ADDR_I32;
                }
                else if (sy->value >= 256)
                {
                    ret = ADDR_I16;
                }
                else
                {
                    ret = ADDR_I8;
                }
            }
            break;
        case SYM_CONST:
            if (sy->value < 256)
            {
                ret = ADDR_I8;
            }
            else if (sy->value < 65536)
            {
                ret = ADDR_I16;
            }
            else
            {
                ret = ADDR_I32;
            }
            break;
        default:
            ret = ADDR_NULL;
            break;
    }

    return ret;
}

/* Output functions */
/* link for as->linking, warning/error/fatal for stderr,
   write_binary for as->output */

static void
link(struct assembler *as, char *format, ...)
{
    if (as->linking != NULL)
    {
        va_list args;
        va_start(args, format);
        if (as->line != NULL)
            fprintf(as->linking, "; %s\n", as->line);
        vfprintf(as->linking, format, args);
        va_end(args);
    }
}

static void
warning(struct assembler *as, char *string)
{
    fprintf(stderr, "\033[1m%s: \033[33m" MSG_WARN "\033[0m %s\n",
            as->filename, string);

    if (as->line_i != NULL)
    {
        fprintf(stderr, " %u (%s) | %s\n", as->line_n + as->line_t,
                as->line_i, as->line);
    }
    else
    {
        fprintf(stderr, " %u | %s\n", as->line_n + as->line_t, as->line);
    }
}

static void
error(struct assembler *as, char *string)
{
    if ((as->line_n + 1) != as->line_err)
    {
        fprintf(stderr, "\033[1m%s: \033[31m" MSG_ERR "\033[0m %s\n",
                as->filename, string);

        if (as->line_i != NULL)
        {
            fprintf(stderr, " %u (%s) | %s\n", as->line_n + as->line_t,
                    as->line_i, as->line);
        }
        else
        {
            fprintf(stderr, " %u | %s\n", as->line_n + as->line_t, as->line);
        }
        as->line_err = as->line_n + 1;
    }
    as->status |= AS_ERROR;
}

static void
fatal(struct assembler *as, char *string)
{
    fprintf(stderr, "\033[1;41m%s:%u: " MSG_FTL " %s\033[0m\n", as->filename,
            as->line_n + as->line_t, string);
    as->line_err = as->line_n + 1;
    as->status |= AS_FATAL;
}

static void
write_binary(struct assembler *as, enum addressing addr, u32 data, char *name)
{
    union { u8 arr[4]; u32 data; } conv = { .data = data};

    if (as->pass == 1)
    {
        switch (addr)
        {
            case ADDR_SYM:
                link(as, ".seek [_ %u +]\n", as->counter);
                link(as, ".field 4 [_ %u +]\n", data);
                break;

            case ADDR_LINK:
                if (name != NULL)
                {
                    link(as, ".seek [_ %u +]\n", as->counter);
                    link(as, ".field 4 %s\n", name);
                }
                else
                {
                    error(as, ERR_BADLINK);
                }
                break;

            default:
                break;
        }
    }

    u8 f = 0;
    switch(addr)
    {
        case ADDR_REG:
        case ADDR_I8:;
            if (as->pass == 1)
                f += (fputc(conv.arr[0], as->output) == EOF) ? 1 : 0;
            as->counter++;
            break;

        case ADDR_I16:;
            if (as->pass == 1)
            {
                f += (fputc(conv.arr[0], as->output) == EOF) ? 1 : 0;
                f += (fputc(conv.arr[1], as->output) == EOF) ? 1 : 0;
            }
            as->counter += 2;
            break;

        case ADDR_I32:
        case ADDR_SYM:
        case ADDR_LINK:;
            if (as->pass == 1)
            {
                f += (fputc(conv.arr[0], as->output) == EOF) ? 1 : 0;
                f += (fputc(conv.arr[1], as->output) == EOF) ? 1 : 0;
                f += (fputc(conv.arr[2], as->output) == EOF) ? 1 : 0;
                f += (fputc(conv.arr[3], as->output) == EOF) ? 1 : 0;
            }
            as->counter += 4;
            break;

        default:
            break;
    }

    if (as->pass == 1)
    {
        if (f != 0)
            fatal(as, ERR_NOOUT);
    }
}

/* Textual parsing functions */
/* Used instead of libc functions to have more control over the
 * maximum size and delimiters */

static void
get_line(struct assembler *as)
{
    u8 com = 0;
    u8 str = 0;

    u32 size = 128;
    as->line = (char *) malloc(size);

    u32 i = 0;
    while (as->line != NULL && as->status < AS_FATAL)
    {
        if (i >= size)
        {
            size *= 2;
            as->line = realloc(as->line, size);
            if (as->line == NULL)
                fatal(as, ERR_NOMEM);
            continue;
        }

        int c = getc(as->input);
        if (c == '\n')
        {
            as->line_n++;
            as->line_t = 0;
        }

        if (com == 0)
        {
            if (c == '\\')
            {
                int c2 = getc(as->input);
                if (c2 == '\n')
                {
                    as->line_n++;
                    as->line_t = 0;
                    continue;
                }
                else
                {
                    as->line[i] = c;
                    i++;
                    as->line[i] = c2;
                }
            }
            else if (c == '\n' || (c == '$' && str == 0))
            {
                if (i == 0)
                {
                    continue;
                }
                else
                {
                    as->line[i] = 0;
                    break;
                }

                if (c == '$')
                    as->line_t = 1;
            }
            else if (c == '\'' || c == '"')
            {
                if (i > 1)
                {
                    if (as->line[i - 1] != '\\' || as->line[i - 2] == '\\')
                        str = !str;
                }
                else
                {
                    str = !str;
                }
                as->line[i] = c;
            }
            else if (c == ';' && str == 0)
            {
                com = 1;
                as->line[i] = 0;
            }
            else if (c == EOF)
            {
                break;
            }
            else if (isprint(c) != 0)
            {
                as->line[i] = c;
            }
            else
            {
                continue;
            }

            i++;
        }
        else if (c == '\n')
        {
            break;
        }
        else if (c == EOF)
        {
            break;
        }
    }
    as->line_c = 0;

    if (i == 0 || as->line == NULL || as->status >= AS_FATAL)
    {
        free(as->line);
        as->line = NULL;
    }
    else
    {
        as->line[i] = 0;
    }
}

static char *
get_token(struct assembler *as)
{
    u32 size = 128;
    char *ret = (char *) malloc(size);
    if (ret == NULL)
    {
        fatal(as, ERR_NOMEM);
    }
    else if (as->line != NULL)
    {
        while (isspace(as->line[as->line_c]) != 0)
            as->line_c++;

        char delim = ' ';
        if (as->line[as->line_c] == '"' || as->line[as->line_c] == '\'')
        {
            delim = as->line[as->line_c];
        }
        else if (as->line[as->line_c] == '[')
        {
            delim = ']';
        }

        u32 i = 0;
        u8 failed = 0;
        if (delim == ' ')
        {
            while (as->status < AS_FATAL)
            {
                char c = as->line[as->line_c + i];
                if (isspace(c) != 0 || c == 0)
                {
                    ret[i] = 0;
                    break;
                }
                else if (isprint(c) != 0)
                {
                    ret[i] = as->line[as->line_c + i];
                }
                else
                {
                    error(as, ERR_BADCHAR);
                    failed = 1;
                    break;
                }
                i++;

                if (i >= size)
                {
                    size *= 2;
                    ret = realloc(ret, size);
                    if (ret == NULL)
                        fatal(as, ERR_NOMEM);
                }
            }
        }
        else
        {
            u8 escape = 0;
            ret[0] = as->line[as->line_c];

            i = 1;
            while (as->status < AS_FATAL)
            {
                char c = as->line[as->line_c + i];
                ret[i] = c;

                if ((c == delim && escape == 0))
                {
                    i++;
                    ret[i] = 0;
                    break;
                }
                else if (c == 0)
                {
                    error(as, ERR_BADTOK);
                    failed = 1;
                    break;
                }
                escape = (c == '\\') ? !escape : 0;
                i++;

                if (i >= size)
                {
                    size *= 2;
                    ret = realloc(ret, size);
                    if (ret == NULL)
                        fatal(as, ERR_NOMEM);
                }
            }
        }
        as->line_c += i;

        if (i == 0 || failed != 0 || as->status >= AS_FATAL)
        {
            free(ret);
            ret = NULL;
        }
    }
    else
    {
        free(ret);
        ret = NULL;
    }

    return ret;
}

static u8
valid_tsym(char *s)
{
    u8 ret = 0;

    if ((s[0] >= '0' && s[0] <= '9') || s[0] == '-')
    {
        ret = 1;
    }
    else
    {
        u32 i = 0;
        while (s[i] == '.')
            i++;

        while (1)
        {
            if (s[i] == 0)
            {
                break;
            }
            else if (s[i] == '.' && (s[i + 1] == '.' || s[i + 1] == 0))
            {
                ret = 1;
                break;
            }
            else if (isalnum(s[i]) == 0 && s[i] != '_' && s[i] != '.')
            {
                ret = 1;
                break;
            }
            i++;
        }
    }

    return ret;
}

static char *
copy_string(char *str)
{
    u32 lenght = strlen(str);

    char *ret = malloc(lenght + 1);
    if (ret != NULL)
        memcpy(ret, str, lenght + 1);

    return ret;
}

/* Macro-wrapped parsing functions */
/* Gives macro capabilities to the parsing functions */

static void
mw_get_line(struct assembler *as)
{
    free(as->line_i);
    as->line_i = NULL;

    if (as->extra_lines->count != 0)
    {
        as->line = as->extra_lines->data[--(as->extra_lines->count)];
        as->line_i = as->lines_info->data[--(as->lines_info->count)];
        as->line_c = 0;
        as->line_t = 0;
    }
    else
    {
        get_line(as);
    }
}

static char *
mw_get_token_core(struct assembler *as)
{
    char *ret = NULL;

    if (as->extra_tokens->count != 0)
    {
        ret = as->extra_tokens->data[--(as->extra_tokens->count)];
    }
    else
    {
        ret = get_token(as);
    }

    if (ret != NULL)
    {
        if (ret[0] == '!' && !(as->in_macro))
        {
            if (ret[1] >= '0' && ret[1] <= '9' && ret[2] == '\0')
            {
                char *arg = as->macro_args[ret[1] - '0'];
                if (arg != NULL)
                {
                    free(ret);
                    ret = malloc(strlen(arg) + 1);
                    if (ret != NULL)
                    {
                        strcpy(ret, arg);
                    }
                    else
                    {
                        fatal(as, ERR_NOMEM);
                    }
                }
                else
                {
                    ret[0] = '\0';
                }
            }
            else if (ret[1] == '#' && ret[2] == '\0')
            {
                u8 i = 0;
                while (i < 10 && as->macro_args[i] != NULL)
                    i++;

                sprintf(ret, "%d", i);
            }
            else if (ret[1] == '@' && ret[2] == '\0')
            {
                u8 count = 0;
                while (count < 10 && as->macro_args[count] != NULL)
                    count++;

                if (count != 0)
                {
                    for (u32 i = count; i > 0; i--)
                    {
                        if (as->macro_args[i - 1] == NULL)
                            break;

                        char *copy = copy_string(as->macro_args[i - 1]);
                        if (copy == NULL ||
                            !(charptr_vec_insert(as->extra_tokens, &copy,
                                                 as->extra_tokens->count)))
                        {
                            fatal(as, ERR_NOMEM);
                            break;
                        }
                    }

                    if (as->status != AS_FATAL)
                    {
                        free(ret);
                        ret = as->extra_tokens
                                           ->data[--(as->extra_tokens->count)];
                    }
                }
                else
                {
                    ret[0] = '\0';
                }
            }
            else
            {
                struct macro dummy = {.name = &(ret[1])};
                struct macro *word = macro_map_search(as->words, &dummy);
                if (word != NULL)
                {
                    for (u32 i = 0; i < word->values->count; i++)
                    {
                        char *copy = copy_string(word->values->data[i]);
                        if (copy == NULL ||
                            !(charptr_vec_insert(as->extra_tokens, &copy,
                                                 as->extra_tokens->count)))
                        {
                            fatal(as, ERR_NOMEM);
                            break;
                        }
                    }

                    if (word->values->count != 0)
                    {
                        free(ret);
                        ret = as->extra_tokens
                                           ->data[--(as->extra_tokens->count)];
                    }
                    else
                    {
                        ret[0] = '\0';
                    }
                }
                else
                {
                    error(as, ERR_BADARG);
                }
            }
        }

        if (ret == NULL)
            fatal(as, ERR_NOMEM);
    }

    return ret;
}

static char *
mw_get_token(struct assembler *as)
{
    char *ret = mw_get_token_core(as);

    while (ret != NULL && ret[0] == '\0')
    {
        free(ret);
        ret = mw_get_token_core(as);
    }

    return ret;
}

/* Symbol functions 2 */
/* Higher level wrappers for table symbols */

static char *
tsym_expand(struct assembler *as, char *string)
{
    char *ret = NULL;

    if (valid_tsym(string) == 0)
    {
        if (string[0] != '.')
        {
            ret = malloc(strlen(string) + 1);
            if (ret != NULL)
            {
                strcpy(ret, string);
            }
            else
            {
                fatal(as, ERR_NOMEM);
            }
        }
        else if (as->context != NULL)
        {
            u32 count = 1;
            while (string[count] == '.')
                count++;

            char *context = malloc(strlen(as->context) + 1);
            if (context != NULL)
            {
                strcpy(context, as->context);

                for (u8 i = ((string[count] != '\0') ? 0 : 1);
                     i < count - 1; i++)
                {
                    char *dot = strrchr(context, '.');
                    if (dot != NULL)
                    {
                        dot[0] = '\0';
                    }
                    else
                    {
                        error(as, ERR_LROOT);

                        free(context);
                        context = NULL;
                        break;
                    }
                }

                if (context != NULL)
                {
                    while (string[0] == '.' &&
                           (string[1] == '.' || string[1] == '\0'))
                    {
                        string = &(string[1]);
                    }

                    ret = malloc(strlen(context) + strlen(string) + 1);
                    if (ret != NULL)
                    {
                        strcpy(ret, context);
                        strcat(ret, string);
                    }
                    else
                    {
                        fatal(as, ERR_NOMEM);
                    }
                }
            }
            else
            {
                fatal(as, ERR_NOMEM);
            }
            free(context);
        }
        else
        {
            error(as, ERR_BADSYM);
        }
    }
    else
    {
        error(as, ERR_BADSYM);
    }

    return ret;
}

static void
tsym_def(struct assembler *as, enum sym_type type, char *string, u32 data)
{
    char *query = tsym_expand(as, string);
    if (query != NULL)
    {
        struct sym dummy = {.string = query};
        struct sym *sy = symbol_map_search(as->symbols, &dummy);
        if (sy == NULL)
        {
            sy = sym_new(type, query, data);
            if (sy == NULL)
            {
                fatal(as, ERR_NOMEM);
            }
            else if (!(symbol_map_insert(as->symbols, sy)))
            {
                fatal(as, ERR_NOMEM);
                sy = sym_del(sy);
            }
            free(sy);
        }
        else
        {
            sy->value = data;
        }
    }
    free(query);
}

static void
tsym_undef(struct assembler *as, char *string)
{
    char *query = tsym_expand(as, string);
    if (query != NULL)
    {
        struct sym dummy = {.string = query};
        struct sym *sy = symbol_map_search(as->symbols, &dummy);
        if (sy != NULL)
            symbol_map_delete(as->symbols, sy);
    }
    else
    {
        error(as, ERR_BADSYM);
    }
    free(query);
}

static struct sym *
tsym_get(struct assembler *as, char *string)
{
    struct sym *ret = NULL;

    char *query = tsym_expand(as, string);
    if (query != NULL)
    {
        struct sym dummy = {.string = query};
        ret = symbol_map_search(as->symbols, &dummy);
        if (ret != NULL)
        {
            ret = sym_new(ret->type, ret->string, ret->value);
        }
        else
        {
            ret = sym_new(SYM_DUMMY, "", 0);
            if (as->pass == 1)
                error(as, ERR_UNKSYM);
        }
    }
    else
    {
        ret = sym_new(SYM_DUMMY, "", 0);
        error(as, ERR_BADSYM);
    }
    free(query);

    return ret;
}

/* Sub parsing functions */
/* Called by the parsing functions */
/* Remember to properly clean rogue symbols created by parse_operand */

static struct sym *math_expression(struct assembler *as, char *string);

static struct sym *
parse_operand(struct assembler *as, char *string)
{
    struct sym *ret = NULL;

    u32 lenght = strlen(string);
    if ((string[0] >= '0' && string[0] <= '9') || string[0] == '-')
    {
        char* endptr;
        u32 value = strtol(string, &endptr, 0);

        ret = sym_new(SYM_CONST, string, value);
        if (*endptr != '\0')
            error(as, ERR_BADNUM);
    }
    else if (string[0] == '\'')
    {
        u8 value = 0;
        if (string[1] == '\\')
        {
            switch (string[2])
            {
                case 'a':
                    value = '\a';
                    break;
                case 'b':
                    value = '\b';
                    break;
                case 'e':
                    value = 0x33;
                    break;
                case 'f':
                    value = '\f';
                    break;
                case 'n':
                    value = '\n';
                    break;
                case 'r':
                    value = '\r';
                    break;
                case 't':
                    value = '\t';
                    break;
                case 'v':
                    value = '\v';
                    break;
                case '\\':
                case '\'':
                case '\"':
                    value = string[2];
                    break;
                default:
                    error(as, ERR_BADESC);
                    break;
            }

            if (string[3] != '\'' || string[4] != 0)
                error(as, ERR_BADCHAR2);
        }
        else if (string[1] != 0 && string[2] == '\'' && string[3] == 0)
        {
            value = string[1];
        }
        else
        {
            error(as, ERR_BADCHAR2);
        }

        ret = sym_new(SYM_CONST, string, value);
    }
    else if (string[0] == '[')
    {
        ret = math_expression(as, string);
    }
    else if (string[0] == '.' && string[1] == 0)
    {
        ret = sym_new(SYM_ADDR, string, as->counter);
    }
    else if (lenght != 0 && string[lenght - 1] == '?')
    {
        string[lenght - 1] = '\0';

        char *query = tsym_expand(as, string);
        if (query != NULL)
        {
            struct sym dummy = {.string = query};

            struct sym *sy = symbol_map_search(as->symbols, &dummy);
            ret = sym_new(SYM_CONST, string, sy != NULL);
        }
        free(query);
    }
    else if (string[0] != 0)
    {
        ret = tsym_get(as, string);
    }

    return ret;
}

static char *
parse_string(struct assembler *as, char *token)
{
    char *ret = NULL;

    if (token != NULL)
    {
        u32 size = 128;
        ret = malloc(size);

        if (ret != NULL)
        {
            u32 lenght = 0;

            u32 i = 0;
            for (i = 1; i < strlen(token) - 1; i++)
            {
                if (token[i] == '\\')
                {
                    i++;

                    char operand[5];
                    operand[0] = '\'';
                    operand[1] = '\\';
                    operand[2] = token[i];
                    operand[3] = '\'';
                    operand[4] = 0;

                    struct sym *c_op = parse_operand(as, operand);
                    ret[lenght++] = (u8)c_op->value;
                    sym_del(c_op);
                }
                else
                {
                    ret[lenght] = token[i];
                    lenght++;
                }

                if (lenght >= size)
                {
                    size *= 2;
                    char *tmp = realloc(ret, size);
                    if (tmp == NULL)
                    {
                        free(ret);
                        ret = NULL;
                        fatal(as, ERR_ENOMEM)
                        break;
                    }
                }
            }
            if (ret != NULL)
                ret[lenght] = 0;
        }
    }

    return ret;
}

/* Symbol arithmethic functions */
/* They are responsible for evaluating math expressions */

#define MATH_SWITCH \
if (token == NULL) \
{ \
    ret = TEMPEST_OP_NULL; \
}

#define MATH_UNARY_CASE(str, op) \
else if (strcmp(str, token) == 0) \
{ \
    inst = TEMPEST_OP_##op##_R; \
}

#define MATH_BINARY_CASE(str, op) \
else if (strcmp(str, token) == 0) \
{ \
    inst = TEMPEST_OP_##op##_R_R; \
}

#define MATH_SWITCH_END \
else \
{ \
    inst = TEMPEST_OP_NULL; \
}

static u8
math_unary_opr(struct tempest *vm, char *token, u32 *idx, u8 *overflow)
{
    u8 ret = 0;

    enum tempest_op inst = TEMPEST_OP_NULL;
    MATH_SWITCH
        MATH_UNARY_CASE("~",  NOT)
        MATH_UNARY_CASE("++", INC)
        MATH_UNARY_CASE("--", DEC)
    MATH_SWITCH_END

    if (inst != TEMPEST_OP_NULL)
    {
        if ((*idx + 6) < vm->regs[TEMPEST_REG_SS])
        {
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = inst;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = TEMPEST_OP_PSW_R;
            vm->data[(*idx)++] = 0;
        }
        else
        {
            *overflow = 1;
        }
    }
    else
    {
        ret = 1;
    }

    return ret;
}

static u8
math_binary_opr(struct tempest *vm, char *token, u32 *idx, u8 *overflow)
{
    u8 ret = 0;

    enum tempest_op inst = TEMPEST_OP_NULL;
    MATH_SWITCH
        MATH_BINARY_CASE("+",   ADD)
        MATH_BINARY_CASE("-",   SUB)
        MATH_BINARY_CASE("*",   MUL)
        MATH_BINARY_CASE("/",   DIV)
        MATH_BINARY_CASE("%",   REM)
        MATH_BINARY_CASE("//",  IDIV)
        MATH_BINARY_CASE("%%",  IREM)
        MATH_BINARY_CASE("&",   AND)
        MATH_BINARY_CASE("|",   OR)
        MATH_BINARY_CASE("^",   XOR)
        MATH_BINARY_CASE("<<",  SLL)
        MATH_BINARY_CASE(">>",  SRL)
        MATH_BINARY_CASE("<<<", SLA)
        MATH_BINARY_CASE(">>>", SRA)
        MATH_BINARY_CASE("=",   EQ)
        MATH_BINARY_CASE("<",   LT)
        MATH_BINARY_CASE(">",   GT)
    MATH_SWITCH_END

    if (inst != TEMPEST_OP_NULL)
    {
        if ((*idx + 9) < vm->regs[TEMPEST_REG_SS])
        {
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 1;
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = inst;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 1;
            vm->data[(*idx)++] = TEMPEST_OP_PSW_R;
            vm->data[(*idx)++] = 0;
        }
        else
        {
            *overflow = 1;
        }
    }
    else
    {
        ret = 1;
    }

    return ret;
}

static u8
math_ternary_opr(struct tempest *vm, char *token, u32 *idx, u8 *overflow)
{
    u8 ret = 0;

    if (strcmp(token, "?") == 0)
    {
        if ((*idx + 29) < vm->regs[TEMPEST_REG_SS])
        {
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 2;
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 1;
            vm->data[(*idx)++] = TEMPEST_OP_PPW_R;
            vm->data[(*idx)++] = 0;

            vm->data[(*idx)++] = TEMPEST_OP_EQ_R_I8;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = TEMPEST_OP_MUL_R_R;
            vm->data[(*idx)++] = 2;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = TEMPEST_OP_EQ_R_I8;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = TEMPEST_OP_MUL_R_R;
            vm->data[(*idx)++] = 1;
            vm->data[(*idx)++] = 0;

            vm->data[(*idx)++] = TEMPEST_OP_MOV_R_I8;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = TEMPEST_OP_ADD_R_R;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 2;
            vm->data[(*idx)++] = TEMPEST_OP_ADD_R_R;
            vm->data[(*idx)++] = 0;
            vm->data[(*idx)++] = 1;
            vm->data[(*idx)++] = TEMPEST_OP_PSW_R;
            vm->data[(*idx)++] = 0;
        }
        else
        {
            *overflow = 1;
        }
    }
    else
    {
        ret = 1;
    }

    return ret;
}

static u8
math_operand(struct assembler *as, struct tempest *vm,
             char *token, u32 *idx, u8 *overflow, u8 *flags)
{
    u8 ret = 0;

    struct sym *t_op = parse_operand(as, token);
    if (t_op != NULL)
    {
        switch (t_op->type)
        {
            case SYM_ADDR:
                *flags |= (1 << 0);
                break;

            case SYM_LINK:
                *flags |= (1 << 1);
                break;

            case SYM_DUMMY:
                *flags |= (1 << 0);
                *flags |= (1 << 1);
                break;

            default:
                break;
        }

        if (as->pass == 0 || t_op->type != SYM_DUMMY)
        {
            if ((*idx + 8) < vm->regs[TEMPEST_REG_SS])
            {
                union {u32 in; u8 out[4];} value = {.in = t_op->value};
                vm->data[(*idx)++] = TEMPEST_OP_MOV_R_I32;
                vm->data[(*idx)++] = 2;
                vm->data[(*idx)++] = value.out[0];
                vm->data[(*idx)++] = value.out[1];
                vm->data[(*idx)++] = value.out[2];
                vm->data[(*idx)++] = value.out[3];
                vm->data[(*idx)++] = TEMPEST_OP_PSW_R;
                vm->data[(*idx)++] = 2;
            }
            else
            {
                *overflow = 1;
            }
        }
        else
        {
            error(as, ERR_UNKSYM);
            ret = 1;
        }
    }
    else
    {
        error(as, ERR_BADOPR);
        ret = 1;
    }
    sym_del(t_op);

    return ret;
}

static void
math_return(struct tempest *vm, u32 *idx, u8 *overflow)
{
    if ((*idx + 2) < vm->regs[TEMPEST_REG_SS])
    {
        vm->data[(*idx)++] = TEMPEST_OP_INT_I8;
        vm->data[(*idx)++] = 0;
    }
    else
    {
        *overflow = 1;
    }
}

static struct sym *
math_expression(struct assembler *as, char *string)
{
    struct sym *ret = NULL;

    u8 flags = 0;

    char *line = as->line;
    u32 line_c = as->line_c;

    u32 length = strlen(string);
    as->line = (char *) malloc(length + 1);
    if (as->line != NULL)
    {
        strcpy(as->line, string);
        as->line[0] = ' ';
        as->line[length - 1] = 0;
        as->line_c = 0;
    }

    struct tempest vm = {0};
    vm.size = 32768;
    vm.data = (uint8_t*) malloc(vm.size);
    if (as->line != NULL  && vm.data != NULL)
    {
        vm.regs[TEMPEST_REG_SS] = vm.size - 1 - 4096;
        vm.regs[TEMPEST_REG_SP] = vm.size - 1;
        vm.regs[TEMPEST_REG_SE] = vm.size - 1;

        u32 idx = 0;
        u8 overflow = 0;

        while (overflow == 0 && as->status < AS_FATAL)
        {
            char *token = mw_get_token(as);
            if (token == NULL)
                break;

            if (math_unary_opr(&vm, token, &idx, &overflow) == 0) {}
            else if (math_binary_opr(&vm, token, &idx, &overflow) == 0) {}
            else if (math_ternary_opr(&vm, token, &idx, &overflow) == 0) {}
            else if (math_operand(as, &vm, token, &idx, &overflow, &flags)
                     != 0)
            {
                free(token);
                break;
            }
            free(token);
        }
        math_return(&vm, &idx, &overflow);

        if (overflow == 1)
        {
            error(as, ERR_EXPL);
        }
        else if (tempest_run(&vm) == 1)
        {
            if ((vm.regs[TEMPEST_REG_SE] - vm.regs[TEMPEST_REG_SP]) == 4)
            {
                union {u8 in[4]; u32 out;} value = {0};
                value.in[0] = vm.data[vm.regs[TEMPEST_REG_SP] + 0];
                value.in[1] = vm.data[vm.regs[TEMPEST_REG_SP] + 1];
                value.in[2] = vm.data[vm.regs[TEMPEST_REG_SP] + 2];
                value.in[3] = vm.data[vm.regs[TEMPEST_REG_SP] + 3];

                if ((flags & (1 << 1)) != 0)
                {
                    ret = sym_new(SYM_LINK, string, value.out);
                }
                else if ((flags & (1 << 0)) != 0)
                {
                    ret = sym_new(SYM_ADDR, string, value.out);
                }
                else
                {
                    ret = sym_new(SYM_CONST, string, value.out);
                }
            }
            else if ((vm.regs[TEMPEST_REG_SE] - vm.regs[TEMPEST_REG_SP]) > 4)
            {
                error(as, ERR_EXPT);
            }
            else
            {
                error(as, ERR_EXPZ);
            }
        }
        else
        {
            error(as, ERR_EXPF);
        }
    }
    else
    {
        fatal(as, ERR_NOMEM);
    }
    free(vm.data);
    free(as->line);

    as->line_c = line_c;
    as->line = line;

    return ret;
}

/* Directives */
/* Called by parse_directive */

static void parse_directive(struct assembler *as, char *token);

static void
directive_if(struct assembler *as, bool elif)
{
    if ((!(as->cond_ip) && !elif) || (as->cond_ip && elif))
    {
        if (!elif)
            as->cond_ip = true;

        char *op = mw_get_token(as);
        if (op != NULL)
        {
            bool clear = false;
            if (elif)
                clear = as->cond_state;

            if (!clear)
            {
                struct sym *t_op = parse_operand(as, op);
                if (t_op != NULL)
                {
                    if (t_op->value == 0)
                        clear = true;
                    sym_del(t_op);
                }
            }

            if (clear)
            {
                char *op2 = NULL;
                do
                {
                    free(op2);
                    op2 = mw_get_token(as);
                    if (op2 == NULL)
                    {
                        free(as->line);
                        mw_get_line(as);
                        if (as->line == NULL)
                            break;
                        op2 = mw_get_token(as);
                    }
                }
                while (op2 != NULL &&
                       strcmp(op2, "%if") != 0 &&
                       strcmp(op2, "%elif") != 0 &&
                       strcmp(op2, "%else") != 0 &&
                       strcmp(op2, "%fi")   != 0);
                if (op2 != NULL)
                    parse_directive(as, op2);

                free(op2);
            }
            else
            {
                as->cond_state = true;
            }
        }
        free(op);
    }
    else
    {
        if (!elif)
        {
            error(as, ERR_MISSFI);
        }
        else
        {
            error(as, ERR_MISSIF);
        }
    }

    char *op2 = mw_get_token(as);
    if (op2 != NULL)
        error(as, ERR_ARGSE);
    free(op2);
}

static void
directive_else(struct assembler *as)
{
    char *op = mw_get_token(as);
    if (op != NULL)
    {
        error(as, ERR_ARGSE);
        free(op);
    }
    free(as->line);
    mw_get_line(as);

    if (as->cond_ip)
    {
        if (as->cond_state)
        {
            char *op2 = NULL;
            do
            {
                free(op2);
                op2 = mw_get_token(as);
                if (op2 == NULL)
                {
                    free(as->line);
                    mw_get_line(as);
                    if (as->line == NULL)
                        break;
                    op2 = mw_get_token(as);
                }
            }
            while (op2 != NULL &&
                   strcmp(op2, "%if") != 0 && strcmp(op2, "%elif") &&
                   strcmp(op2, "%else") != 0 && strcmp(op2, "%fi") != 0);
            if (op2 != NULL)
                parse_directive(as, op2);

            free(op2);
        }
        else
        {
            as->cond_state = true;
        }
    }
    else
    {
        error(as, ERR_MISSIF);
    }
}

static void
directive_fi(struct assembler *as)
{
    if (as->cond_ip)
    {
        as->cond_ip = false;
        as->cond_state = false;
    }
    else
    {
        error(as, ERR_MISSIF);
    }

    char *op2 = mw_get_token(as);
    if (op2 != NULL)
        error(as, ERR_ARGSE);
    free(op2);
}

static void
directive_include(struct assembler *as)
{
    u32 includes = 0;

    FILE *input = as->input;
    while (1)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        if (op[0] != '"' || op[strlen(op) - 1] != '"')
        {
            free(op);
            error(as, ERR_BADSTR);
            break;
        }

        char *file = parse_string(as, op);
        if (file != NULL)
        {
            char *line = as->line;
            u32 line_c = as->line_c;
            u32 line_t = as->line_t;

            u32 count = as->extra_lines->count - includes;
            as->input = fopen(file, "r");
            if (as->input != NULL)
            {
                u32 info_l = strlen(file) + 1 + 10 + 1;
                u32 line_n = 1;
                while (1)
                {
                    get_line(as);
                    if (as->line != NULL)
                    {
                        char *info = malloc(info_l);
                        sprintf(info, "%s:%u", file, line_n);

                        if (!(charptr_vec_insert(as->extra_lines, &as->line,
                                                 count) &&
                             charptr_vec_insert(as->lines_info, &info, count)))
                        {
                            free(as->line);
                            fatal(as, ERR_NOMEM);
                            break;
                        }
                        as->line = NULL;
                        as->line_n--;

                        line_n++;
                        includes++;
                    }
                    else
                    {
                        break;
                    }
                }
                fclose(as->input);
            }
            else
            {
                error(as, ERR_OREAD);
                break;
            }

            as->line = line;
            as->line_c = line_c;
            as->line_t = line_t;
        }
        else
        {
            error(as, ERR_NOMEM);
            break;
        }
        free(file);
        free(op);
    }
    as->input = input;
}

static void
directive_macro(struct assembler *as, bool weak)
{
    char *name = NULL;

    char *op = mw_get_token(as);
    char *op2 = mw_get_token(as);
    if (op != NULL && op2 == NULL)
    {
        name = tsym_expand(as, op);
    }
    else if (op2 != NULL)
    {
        error(as, ERR_ARGSE);
    }
    free(op);
    free(op2);

    struct macro new = {.name = name};
    if (new.name != NULL)
    {
        new.values = charptr_vec_alloc(16);
        if (new.values == NULL)
        {
            fatal(as, ERR_NOMEM);
            free(name);
        }
    }
    else
    {
        error(as, ERR_ARGSM);
    }

    free(as->line);
    as->in_macro = true;
    while (1)
    {
        mw_get_line(as);
        if (as->line != NULL)
        {
            op2 = mw_get_token(as);
            if (strcmp(op2, "%endmacro") == 0)
            {
                free(op2);

                op2 = mw_get_token(as);
                if (op2 != NULL)
                    error(as, ERR_ARGSE);
                free(op2);

                free(as->line);
                as->line = NULL;
                break;
            }
            else
            {
                if (new.name != NULL && !weak)
                {
                    charptr_vec_prepend(new.values, &as->line);
                }
                else
                {
                    free(as->line);
                }
            }
            free(op2);
        }
        else
        {
            error(as, ERR_MISSEND);
            break;
        }
    }

    if (new.values != NULL && !weak)
    {
        struct macro *old = macro_map_search(as->macros, &new);
        if (old != NULL)
            macro_map_delete(as->macros, old);

        if (!(macro_map_insert(as->macros, &new)))
            fatal(as, ERR_NOMEM);
    }
    else
    {
        free(new.name);
        charptr_vec_free(new.values);
    }

    as->in_macro = false;
}

static void
directive_endmacro(struct assembler *as)
{
    error(as, ERR_MISSSTA);
}

static void
directive_word(struct assembler *as, bool weak)
{
    char *name = NULL;

    char *op = mw_get_token(as);
    if (op != NULL)
        name = tsym_expand(as, op);
    free(op);

    struct macro new = {.name = name};
    if (new.name != NULL && !weak)
    {
        new.values = charptr_vec_alloc(16);
        if (new.values == NULL)
        {
            fatal(as, ERR_NOMEM);
            free(name);
        }
    }
    else if (new.name == NULL)
    {
        error(as, ERR_ARGSM);
    }

    while (1)
    {
        char *op2 = mw_get_token(as);
        if (op2 != NULL)
        {
            if (new.name != NULL && !weak)
            {
                charptr_vec_prepend(new.values, &op2);
            }
            else
            {
                free(op2);
            }
        }
        else
        {
            break;
        }
    }

    if (new.values != NULL && !weak)
    {
        struct macro *old = macro_map_search(as->words, &new);
        if (old != NULL)
            macro_map_delete(as->words, old);

        if (!(macro_map_insert(as->words, &new)))
            fatal(as, ERR_NOMEM);
    }
    else
    {
        free(new.name);
        charptr_vec_free(new.values);
    }
}

static void
directive_weak(struct assembler *as)
{
    char *op = mw_get_token(as);
    if (op != NULL)
    {
        if (strcmp(op, "macro") == 0)
        {
            u32 line_c = as->line_c;

            struct macro dummy = {.name = mw_get_token(as)};
            struct macro *m = macro_map_search(as->macros, &dummy);
            as->line_c = line_c;
            directive_macro(as, m != NULL);
            free(dummy.name);
        }
        else if (strcmp(op, "word") == 0)
        {
            u32 line_c = as->line_c;

            struct macro dummy = {.name = mw_get_token(as)};
            struct macro *m = macro_map_search(as->words, &dummy);
            as->line_c = line_c;
            directive_word(as, m != NULL);
            free(dummy.name);
        }
        else
        {
            error(as, ERR_UNKDIR);
        }
    }
    else
    {
        error(as, ERR_ARGSM);
    }
    free(op);
}

static void
directive_undef(struct assembler *as)
{
    char *op = mw_get_token(as);
    if (op != NULL)
    {
        if (strcmp(op, "macro") == 0)
        {
            struct macro dummy = {.name = mw_get_token(as)};
            if (dummy.name != NULL)
            {
                struct macro *m = macro_map_search(as->macros, &dummy);
                if (m != NULL)
                    macro_map_delete(as->macros, m);
            }
            free(dummy.name);
        }
        else if (strcmp(op, "word") == 0)
        {
            struct macro dummy = {.name = mw_get_token(as)};
            if (dummy.name != NULL)
            {
                struct macro *m = macro_map_search(as->words, &dummy);
                if (m != NULL)
                    macro_map_delete(as->words, m);
            }
            free(dummy.name);
        }
        else
        {
            error(as, ERR_UNKDIR);
        }
    }
    else
    {
        error(as, ERR_ARGSM);
    }
    free(op);
}

/* Pseudo Instructions */
/* Called by parse_pseudo */

static void
pseudo_align(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        struct sym *t_op = parse_operand(as, op);
        if (t_op != NULL)
        {
            if (t_op->value != 0)
            {
                while ((as->counter % t_op->value) != 0 && \
                       as->status < AS_FATAL)
                    write_binary(as, ADDR_I8, 0, NULL);
            }
        }
        sym_del(t_op);
        free(op);
    }
}

static void
pseudo_field(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        struct sym *t_op = parse_operand(as, op);
        struct sym *t_op2 = parse_operand(as, op2);
        if (t_op != NULL && t_op2 != NULL)
        {
            if (t_op->value != 0)
            {
                union { u8 arr[4]; u32 data; } conv = { .data = t_op2->value };

                i32 bytes = t_op->value;
                if (bytes > 0)
                {
                    for (i32 i = 0; i < bytes && as->status < AS_FATAL; i++)
                    {
                        write_binary(as, ADDR_I8,
                                     (i < 4) ? conv.arr[i] : 0, NULL);
                    }
                }
                else if (bytes < 0)
                {
                    for (i32 i = ++bytes; i <= 0 && as->status < AS_FATAL; i++)
                    {
                        write_binary(as, ADDR_I8,
                                     (i > -4) ? conv.arr[-i] : 0, NULL);
                    }
                }
            }
        }
        sym_del(t_op);
        sym_del(t_op2);
        free(op);
        free(op2);
    }
}

static void
pseudo_pad(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        struct sym *t_op = parse_operand(as, op);
        if (t_op != NULL)
        {
            u32 i = 0;
            for (i = 0; i < t_op->value && as->status < AS_FATAL; i++)
                write_binary(as, ADDR_I8, 0, NULL);
        }
        sym_del(t_op);
        free(op);
    }
}

static void
pseudo_seek(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        struct sym *t_op = parse_operand(as, op);
        if (t_op != NULL)
        {
            u32 pos = ftell(as->output);
            u32 pos2 = pos + (t_op->value - as->counter);
            if (t_op->value > as->counter)
            {
                if (as->pass == 1)
                {
                    if (fseek(as->output, pos2, SEEK_SET) != 0)
                    {
                        for (u32 i = 0; i < pos2 &&
                             as->status < AS_FATAL; i++)
                        {
                            write_binary(as, ADDR_I8, 0, NULL);
                        }
                    }
                }
            }
            else if (t_op->value < as->counter)
            {
                if (as->pass == 1)
                {
                    if (pos >= (as->counter - t_op->value))
                    {
                        if (fseek(as->output, pos2, SEEK_SET) != 0)
                            fatal(as, ERR_SEEKB);
                    }
                    else
                    {
                        fatal(as, ERR_SEEKB);
                    }
                }
            }
            as->counter = t_op->value;
        }
        sym_del(t_op);
        free(op);
    }
}

static void
pseudo_define(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        struct sym *t_op2 = parse_operand(as, op2);
        if (t_op2 != NULL)
        {
            tsym_def(as, t_op2->type, op, t_op2->value);
            if (as->pass == 1)
            {
                if (t_op2->type == SYM_ADDR)
                {
                    char *name = tsym_expand(as, op);
                    link(as, ".define %s [_ %u +]\n", name, t_op2->value);
                    free(name);
                }
                else if (t_op2->type == SYM_LINK)
                {
                    link(as, ".define %s %s\n", op, op2);
                }
            }
        }
        sym_del(t_op2);

        free(op);
        free(op2);
    }
}

static void
pseudo_weak(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        struct sym dummy = {.string = op};
        if (symbol_map_search(as->symbols, &dummy) == NULL)
        {
            struct sym *t_op2 = parse_operand(as, op2);
            if (t_op2 != NULL)
            {
                tsym_def(as, t_op2->type, op, t_op2->value);
                if (as->pass == 1)
                {
                    if (t_op2->type == SYM_ADDR)
                    {
                        char *name = tsym_expand(as, op);
                        link(as, ".define %s [_ %u +]\n", name, t_op2->value);
                        free(name);
                    }
                    else if (t_op2->type == SYM_LINK)
                    {
                        link(as, ".define %s %s\n", op, op2);
                    }
                }
            }
            sym_del(t_op2);
        }

        free(op);
        free(op2);
    }
}

static void
pseudo_undef(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        tsym_undef(as, op);
        free(op);
    }
}

static void
pseudo_assert(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        if (as->pass == 0)
        {
            struct sym *t_op = parse_operand(as, op);
            struct sym *t_op2 = parse_operand(as, op2);
            if (t_op != NULL && t_op2 != NULL)
            {
                if (t_op->value != t_op2->value)
                    error(as, ERR_ASSERT);
            }
            sym_del(t_op);
            sym_del(t_op2);
        }
        free(op);
        free(op2);
    }
}

static void
pseudo_debug(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        if (as->pass == 1)
        {
            struct sym *t_op = parse_operand(as, op);
            if (t_op != NULL)
            {
                if ((as->line_n + 1) != as->line_err)
                {
                    fprintf(stderr, "\033[1m%s: \033[32m" MSG_DBG,
                            as->filename);

                    if (t_op->type != SYM_DUMMY)
                    {
                        fprintf(stderr, " \033[0;34m%s",
                                (as->context != NULL) ?
                                as->context : "(root)");

                        fprintf(stderr, " \033[0;36m");
                        switch (t_op->type)
                        {
                            case SYM_REG:
                                fprintf(stderr, DBG_REG);
                                break;
                            case SYM_ADDR:
                                fprintf(stderr, DBG_ADDR);
                                break;
                            case SYM_CONST:
                                fprintf(stderr, DBG_CONS);
                                break;
                            case SYM_LINK:
                                fprintf(stderr, DBG_LINK);
                                break;
                            default:
                                fprintf(stderr, DBG_INV);
                                break;
                        }
                        fprintf(stderr, " \033[0;32m0x%x", t_op->value);
                        fprintf(stderr, " \033[0m\n");

                        if (as->line_i != NULL)
                        {
                            fprintf(stderr, " %u (%s) | %s\n",
                                    as->line_n + as->line_t, as->line_i,
                                    as->line);
                        }
                        else
                        {
                            fprintf(stderr, " %u | %s\n",
                                    as->line_n + as->line_t, as->line);
                        }
                    }
                    else
                    {
                        error(as, ERR_UNKSYM);
                    }
                }
            }
            sym_del(t_op);
        }
        free(op);
    }
}

static void
pseudo_counter(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        struct sym *t_op = parse_operand(as, op);
        if (t_op != NULL)
            as->counter = t_op->value;
        sym_del(t_op);

        free(op);
    }
}

static void
pseudo_call(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        struct sym *t_op = parse_operand(as, op);
        if (t_op != NULL)
        {
            enum addressing addr = addr_type(as, t_op);

            u32 after = as->counter + 10;
            switch (addr)
            {
                case ADDR_I16:
                    after += 1;
                    break;
                case ADDR_I32:
                case ADDR_LINK:
                    after += 3;
                    break;
                default:
                    break;
            }

            char lbuf[32] = {0};
            sprintf(lbuf, "[_ %u +]", after);
            write_binary(as, ADDR_I8, TEMPEST_OP_MOV_R_I32, NULL);
            write_binary(as, ADDR_I8, TEMPEST_REG_R15, NULL);
            write_binary(as, ADDR_LINK, after, lbuf);

            write_binary(as, ADDR_I8, TEMPEST_OP_PSW_R, NULL);
            write_binary(as, ADDR_I8, TEMPEST_REG_R15, NULL);

            switch (addr)
            {
                case ADDR_REG:
                    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_R, NULL);
                    write_binary(as, ADDR_I8, t_op->value, NULL);
                    break;

                case ADDR_I8:
                    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_I8, NULL);
                    write_binary(as, ADDR_I8, t_op->value, NULL);
                    break;
                case ADDR_I16:
                    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_I16, NULL);
                    write_binary(as, ADDR_I16, t_op->value, NULL);
                    break;
                case ADDR_I32:
                case ADDR_SYM:
                    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_I32, NULL);
                    write_binary(as, addr, t_op->value, NULL);
                    break;

                case ADDR_LINK:
                    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_I32, NULL);
                    write_binary(as, ADDR_LINK, t_op->value, op);
                    break;

                default:
                    break;
            }
        }
        sym_del(t_op);

        free(op);
    }
}

static void
pseudo_return(struct assembler *as)
{
    write_binary(as, ADDR_I8, TEMPEST_OP_PPW_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_R15, NULL);

    write_binary(as, ADDR_I8, TEMPEST_OP_JMP_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_R15, NULL);
}

static void
pseudo_enter(struct assembler *as)
{
    write_binary(as, ADDR_I8, TEMPEST_OP_PSW_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SE, NULL);

    write_binary(as, ADDR_I8, TEMPEST_OP_MOV_R_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SE, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SP, NULL);
}

static void
pseudo_leave(struct assembler *as)
{
    write_binary(as, ADDR_I8, TEMPEST_OP_MOV_R_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SP, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SE, NULL);

    write_binary(as, ADDR_I8, TEMPEST_OP_ADD_R_I8, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SE, NULL);
    write_binary(as, ADDR_I8, 4, NULL);

    write_binary(as, ADDR_I8, TEMPEST_OP_PPW_R, NULL);
    write_binary(as, ADDR_I8, TEMPEST_REG_SE, NULL);
}

static void
pseudo_save(struct assembler *as)
{
    for (enum tempest_reg i = TEMPEST_REG_R0; i <= TEMPEST_REG_R15; i++)
    {
        write_binary(as, ADDR_I8, TEMPEST_OP_PSW_R, NULL);
        write_binary(as, ADDR_I8, i, NULL);
    }
}

static void
pseudo_load(struct assembler *as)
{
    for (enum tempest_reg i = TEMPEST_REG_R15;
         (signed) i >= TEMPEST_REG_R0; i--)
    {
        write_binary(as, ADDR_I8, TEMPEST_OP_PPW_R, NULL);
        write_binary(as, ADDR_I8, i, NULL);
    }
}

static void
pseudo_embed(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        if (op[0] != '"' || op[strlen(op) - 1] != '"')
        {
            error(as, ERR_BADSTR);
            break;
        }

        char *path = parse_string(as, op);
        if (path != NULL)
        {
            FILE *embed = fopen(path, "rb");
            if (embed != NULL)
            {
                if (as->pass == 0)
                {
                    if (fseek(embed, 0, SEEK_END) == 0)
                    {
                        as->counter += ftell(embed);
                    }
                    else
                    {
                        error(as, ERR_FSIZE);
                    }
                }
                else
                {
                    u32 count = 0;
                    u8 buf[4096] = {0};
                    while ((count = fread(buf, 1, 4096, embed)) != 0)
                    {
                        fwrite(buf, count, 1, as->output);
                        as->counter += count;
                    }
                }

                fclose(embed);
            }
            else
            {
                error(as, ERR_OREAD);
            }
        }
        else
        {
            fatal(as, ERR_NOMEM);
        }
        free(path);
        free(op);
    }
}

static void
pseudo_string(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        if (op2[0] != '"' || op2[strlen(op2) - 1] != '"')
        {
            error(as, ERR_BADSTR);
            break;
        }

        if (strcmp(op, "nt") == 0)
        {
            char *str = parse_string(as, op2);
            if (str != NULL)
            {
                for (u32 i = 0; i < strlen(str) + 1; i++)
                    write_binary(as, ADDR_I8, str[i], NULL);
            }
            else
            {
                fatal(as, ERR_NOMEM);
            }
            free(str);
        }
        else
        {
            struct sym *t_op = parse_operand(as, op);
            if (t_op != NULL)
            {
                u32 pad = (((i32) t_op->value) > 0) ? t_op->value :
                                                    -(t_op->value);
                for (u32 i = 0; i < pad && as->status < AS_FATAL; i++)
                    write_binary(as, ADDR_I8, 0, NULL);

                u32 start = as->counter;
                char *str = parse_string(as, op2);
                if (str != NULL)
                {
                    for (u32 i = 0; i < strlen(str); i++)
                        write_binary(as, ADDR_I8, str[i], NULL);
                }
                else
                {
                    fatal(as, ERR_NOMEM);
                }
                u32 end = as->counter;

                u32 length = end - start;
                as->counter = start - pad;
                if (as->pass == 0 ||
                    fseek(as->output, as->counter, SEEK_SET) == 0)
                {
                    union { u8 arr[4]; u32 data; } conv = { .data = length };

                    i32 bytes = t_op->value;
                    if (bytes > 0)
                    {
                        for (i32 i = 0; i < bytes &&
                                        as->status < AS_FATAL; i++)
                        {
                            write_binary(as, ADDR_I8,
                                         (i < 4) ? conv.arr[i] : 0, NULL);
                        }
                    }
                    else if (bytes < 0)
                    {
                        for (i32 i = ++bytes; i <= 0 &&
                                              as->status < AS_FATAL; i++)
                        {
                            write_binary(as, ADDR_I8,
                                         (i > -4) ? conv.arr[-i] : 0, NULL);
                        }
                    }

                    as->counter = end;
                    if (as->pass == 1 && fseek(as->output, end, SEEK_SET) != 0)
                        fatal(as, ERR_SEEKB);
                }
                else
                {
                    fatal(as, ERR_SEEKB);
                }
            }
            sym_del(t_op);
        }
        free(op);
        free(op2);
    }
}

static void
pseudo_import(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        if (as->pass == 0)
            tsym_def(as, SYM_LINK, op, 0);

        free(op);
    }
}

static void
pseudo_export(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        char *op2 = mw_get_token(as);
        if (op == NULL)
            break;
        if (op2 == NULL)
        {
            free(op);
            error(as, ERR_ARGSM);
            break;
        }

        if (as->pass == 1)
            link(as, ".define %s %s\n", op, op2);

        free(op);
        free(op2);
    }
}

static void
pseudo_context(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        char *op = mw_get_token(as);
        if (op == NULL)
            break;

        char *name = tsym_expand(as, op);
        free(as->context);
        as->context = name;

        free(op);
    }
}

/* Parsing functions */
/* Called by the execution functions */

static void
parse_label(struct assembler *as, char* token)
{
    u32 length = strlen(token);
    char *label = malloc(length + 1);
    if (label != NULL)
    {
        strcpy(label, token);
        label[length - 1] = 0;

        if (strcmp(label, "..") != 0)
        {
            char *name = tsym_expand(as, label);
            if (name != NULL)
            {
                free(as->context);
                as->context = name;
                if (as->pass == 0)
                {
                    tsym_def(as, SYM_ADDR, name, as->counter);
                }
                else
                {
                    link(as, ".define %s [_ %u +]\n", name, as->counter);
                }
            }
        }
        else
        {
            char *dot = NULL;
            if (as->context != NULL)
            {
                dot = strrchr(as->context, '.');
                if (dot != NULL)
                {
                    *dot = 0;
                }
                else
                {
                    free(as->context);
                    as->context = NULL;
                }
            }
            else
            {
                error(as, ERR_LROOT);
            }
        }
    }
    else
    {
        fatal(as, ERR_NOMEM);
    }
    free(label);
}

static void
parse_directive(struct assembler *as, char *token)
{
    if (strcmp(token, "%if") == 0)
    {
        directive_if(as, false);
    }
    else if (strcmp(token, "%elif") == 0)
    {
        directive_if(as, true);
    }
    else if (strcmp(token, "%else") == 0)
    {
        directive_else(as);
    }
    else if (strcmp(token, "%fi") == 0)
    {
        directive_fi(as);
    }
    else if (strcmp(token, "%warn") == 0)
    {
        if (as->pass == 1)
            warning(as, &(as->line[as->line_c + 1]));
        as->line_c = strlen(as->line);
    }
    else if (strcmp(token, "%error") == 0)
    {
        error(as, &(as->line[as->line_c + 1]));
        as->line_c = strlen(as->line);
    }
    else if (strcmp(token, "%args") == 0)
    {
        for (u8 i = 0; i < 10; i++)
        {
            free(as->macro_args[i]);
            as->macro_args[i] = mw_get_token(as);
        }

        char *op2 = mw_get_token(as);
        if (op2 != NULL)
            error(as, ERR_ARGSE);
        free(op2);
    }
    else if (strcmp(token, "%include") == 0)
    {
        directive_include(as);
    }
    else if (strcmp(token, "%macro") == 0)
    {
        directive_macro(as, false);
    }
    else if (strcmp(token, "%endmacro") == 0)
    {
        directive_endmacro(as);
    }
    else if (strcmp(token, "%word") == 0)
    {
        directive_word(as, false);
    }
    else if (strcmp(token, "%weak") == 0)
    {
        directive_weak(as);
    }
    else if (strcmp(token, "%undef") == 0)
    {
        directive_undef(as);
    }
    else
    {
        error(as, ERR_UNKDIR);
    }
}

static void
parse_pseudo(struct assembler *as, char *token)
{
    if (strcmp(token, ".align") == 0)
    {
        pseudo_align(as);
    }
    else if (strcmp(token, ".field") == 0)
    {
        pseudo_field(as);
    }
    else if (strcmp(token, ".pad") == 0)
    {
        pseudo_pad(as);
    }
    else if (strcmp(token, ".seek") == 0)
    {
        pseudo_seek(as);
    }
    else if (strcmp(token, ".define") == 0)
    {
        pseudo_define(as);
    }
    else if (strcmp(token, ".weak") == 0)
    {
        pseudo_weak(as);
    }
    else if (strcmp(token, ".undef") == 0)
    {
        pseudo_undef(as);
    }
    else if (strcmp(token, ".assert") == 0)
    {
        pseudo_assert(as);
    }
    else if (strcmp(token, ".debug") == 0)
    {
        pseudo_debug(as);
    }
    else if (strcmp(token, ".counter") == 0)
    {
        pseudo_counter(as);
    }
    else if (strcmp(token, ".call") == 0)
    {
        pseudo_call(as);
    }
    else if (strcmp(token, ".return") == 0)
    {
        pseudo_return(as);
    }
    else if (strcmp(token, ".enter") == 0)
    {
        pseudo_enter(as);
    }
    else if (strcmp(token, ".leave") == 0)
    {
        pseudo_leave(as);
    }
    else if (strcmp(token, ".save") == 0)
    {
        pseudo_save(as);
    }
    else if (strcmp(token, ".load") == 0)
    {
        pseudo_load(as);
    }
    else if (strcmp(token, ".embed") == 0)
    {
        pseudo_embed(as);
    }
    else if (strcmp(token, ".string") == 0)
    {
        pseudo_string(as);
    }
    else if (strcmp(token, ".import") == 0)
    {
        pseudo_import(as);
    }
    else if (strcmp(token, ".export") == 0)
    {
        pseudo_export(as);
    }
    else if (strcmp(token, ".context") == 0)
    {
        pseudo_context(as);
    }
    else
    {
        error(as, ERR_UNKPIN);
    }

    while (as->line[as->line_c] != 0)
    {
        if (isspace(as->line[as->line_c]) == 0)
            error(as, ERR_ARGSE);
        as->line_c++;
    }
}

static void
parse_instruction(struct assembler *as, char* token)
{
    u32 length = strlen(token);

    char *code = malloc(1 + length + (4 * 2) + 1);
    if (code != NULL)
    {
        strcpy(code, "@");
        strcat(code, token);

        u8 idx = 0;
        char *str[2] = {0};
        u32 data[2] = {0};
        enum addressing addr[2] = {0};
        for (idx = 0; idx < 2; idx++)
        {
            char *op = mw_get_token(as);
            if (op == NULL)
                break;

            str[idx] = op;
            struct sym *t_op = NULL;
            if (op[0] != '\0' && op[1] == ':')
            {
                t_op = parse_operand(as, &(op[2]));
                switch (op[0])
                {
                    case 'r':
                        addr[idx] = ADDR_REG;
                        break;
                    case 'b':
                        addr[idx] = ADDR_I8;
                        break;
                    case 'h':
                        addr[idx] = ADDR_I16;
                        break;
                    case 'w':
                        addr[idx] = ADDR_I32;
                        break;
                    default:
                        error(as, ERR_UNKADDR);
                        sym_del(t_op);
                        t_op = NULL;
                        break;
                }
            }
            else
            {
                t_op = parse_operand(as, op);
                if (t_op != NULL)
                    addr[idx] = addr_type(as, t_op);
            }

            if (t_op != NULL)
            {
                strcat(code, addr_suffixes[addr[idx]]);
                data[idx] = t_op->value;
            }
            sym_del(t_op);
        }

        struct sym dummy = {.string = code};
        struct sym *inst = symbol_map_search(as->symbols, &dummy);
        if (inst != NULL)
        {
            if (inst->type == SYM_INST)
            {
                if (as->status < AS_FATAL)
                    write_binary(as, ADDR_I8, inst->value, NULL);

                for (u8 i = 0; i < idx && as->status < AS_FATAL; i++)
                    write_binary(as, addr[i], data[i], str[i]);
            }
            else
            {
                error(as, ERR_UNKINS);
            }
        }
        else
        {
            error(as, ERR_UNKINS);
        }
        for (u8 i = 0; i < idx; i++)
            free(str[i]);

        while (as->line[as->line_c] != 0)
        {
            if (isspace(as->line[as->line_c]) == 0)
                error(as, ERR_ARGSE);
            as->line_c++;
        }
    }
    free(code);
}

static void
parse_macro(struct assembler *as, char *token)
{
    char *name = tsym_expand(as, &(token[1]));
    if (name != NULL)
    {
        char space = ' ';
        char directive[] = "%args";

        struct char_vec_t *args = char_vec_alloc(128);
        char_vec_append_bulk(args, directive, sizeof(directive) - 1);
        for (u32 i = 0; i < 10; i++)
        {
            if (as->macro_args[i] == NULL)
                break;

            char_vec_append(args, &space);
            char_vec_append_bulk(args, as->macro_args[i],
                                 strlen(as->macro_args[i]));
        }

        struct char_vec_t *args2 = char_vec_alloc(128);
        char_vec_append_bulk(args2, directive, sizeof(directive) - 1);
        while (1)
        {
            char *op2 = mw_get_token(as);
            if (op2 == NULL)
                break;

            char_vec_append(args2, &space);
            char_vec_append_bulk(args2, op2, strlen(op2));
            free(op2);
        }

        struct macro dummy = {.name = name};
        struct macro *m = NULL;

        char *line = as->line;
        u32 line_c = as->line_c;
        u32 line_t = as->line_t;

        char *op2 = mw_get_token(as);
        if (op2 == NULL)
        {
            m = macro_map_search(as->macros, &dummy);
            if (m == NULL)
                error(as, ERR_UNKMACRO);
        }
        else
        {
            error(as, ERR_ARGSE);
        }
        free(op2);
        free(name);

        if (m != NULL)
        {
            u32 count = as->extra_lines->count;
            char *extra = copy_string(args->data);
            char *info = copy_string(line);
            if (!(charptr_vec_insert(as->extra_lines, &extra, count) &&
                  charptr_vec_insert(as->lines_info, &info, count)))
            {
                fatal(as, ERR_NOMEM);
            }

            for (u32 i = 0; i < m->values->count &&
                            as->status != AS_FATAL; i++)
            {
                count = as->extra_lines->count;
                extra = copy_string(m->values->data[i]);
                info = copy_string(line);
                if (extra != NULL && info != NULL)
                {
                    if (!(charptr_vec_insert(as->extra_lines, &extra, count) &&
                          charptr_vec_insert(as->lines_info, &info, count)))
                    {
                        fatal(as, ERR_NOMEM);
                    }
                }
                else
                {
                    free(extra);
                    free(info);
                    fatal(as, ERR_NOMEM);
                }
            }

            count = as->extra_lines->count;
            extra = copy_string(args2->data);
            info = copy_string(line);
            if (!(charptr_vec_insert(as->extra_lines, &extra, count) &&
                  charptr_vec_insert(as->lines_info, &info, count)))
            {
                fatal(as, ERR_NOMEM);
            }
        }
        char_vec_free(args);
        char_vec_free(args2);

        as->line = line;
        as->line_c = line_c;
        as->line_t = line_t;
    }
}


/* Execution functions */
/* Called by the main function */

static u8
init(struct assembler *as)
{
    u32 ret = 0;

    as->symbols = symbol_map_alloc(1024);

    as->extra_lines = charptr_vec_alloc(128);
    as->lines_info = charptr_vec_alloc(128);
    as->extra_tokens = charptr_vec_alloc(128);

    as->macros = macro_map_alloc(32);
    as->words = macro_map_alloc(32);

    if (as->symbols != NULL &&
        as->extra_lines != NULL && as->lines_info != NULL &&
        as->macros != NULL)
    {
        u8 i = 0;
        char buf[32] = "";
        struct sym *sy = NULL;
        #define INSERT_KEYWORD(type, string, value) \
            strcpy(buf, string); \
            for (i = 0; i < 32; i++) \
            { \
                if (buf[i] == 0) \
                    break; \
                buf[i] = tolower(buf[i]); \
            } \
            sy = sym_new(type, buf, value); \
            ret += !(symbol_map_insert(as->symbols, sy)); \
            free(sy);

        #define INSERT_OP(addr, macro) \
            INSERT_KEYWORD(SYM_INST, "@" #macro "_" #addr, \
                           TEMPEST_OP_##macro##_##addr)
        TEMPEST_MACRO_ALL_OPCODES(INSERT_OP)

        #define INSERT_REG(id, name) \
            INSERT_KEYWORD(SYM_REG, #name, id)
        TEMPEST_MACRO_ALL_REGISTERS(INSERT_REG)
    }
    else
    {
        ret = 1;
    }

    return (u8)(ret > 0);
}

static void
cleanup(struct assembler *as)
{
    symbol_map_free(as->symbols);

    macro_map_free(as->macros);
    macro_map_free(as->words);

    charptr_vec_free(as->extra_lines);
    charptr_vec_free(as->lines_info);
    charptr_vec_free(as->extra_tokens);

    free(as->context);

    for (u8 i = 0; i < 10; i++)
        free(as->macro_args[i]);
}

static void
execute(struct assembler *as)
{
    while (as->status < AS_FATAL)
    {
        mw_get_line(as);
        if (as->line == NULL)
            break;

        while (as->status < AS_FATAL)
        {
            char *token = mw_get_token(as);
            if (token == NULL)
                break;

            size_t size = strlen(token);
            if (size != 0 && token[size - 1] == ':')
            {
                parse_label(as, token);
            }
            else if (token[0] == '%')
            {
                parse_directive(as, token);
            }
            else if (token[0] == '.')
            {
                parse_pseudo(as, token);
            }
            else if (token[0] == '@')
            {
                parse_macro(as, token);
            }
            else
            {
                parse_instruction(as, token);
            }
            free(token);
        }
        free(as->line);
    }
}

static u8
usage(void)
{
    fprintf(stderr, MSG_USAGE "\n");
    return 1;
}

int main(int argc, char* argv[])
{
    struct assembler as = {0};
    if (argc == 2 || argc == 3 || argc == 4)
    {
        if (init(&as) != 0)
        {
            fatal(&as, ERR_INIT);
        }
        else
        {
            as.filename = (argc >= 3) ? argv[2] : "a.out";
            if (strcmp(as.filename, argv[1]) != 0)
            {
                if (argc == 4)
                {
                    if (strcmp(argv[3], argv[1]) != 0 &&
                        strcmp(argv[3], argv[2]) != 0)
                    {
                        as.linking = fopen(argv[3], "w");
                        if (as.linking == NULL)
                            error(&as, ERR_OWRIT);
                    }
                    else
                    {
                        error(&as, ERR_FSAME);
                    }
                }

                if (as.status == AS_OK)
                {
                    as.output = fopen(as.filename, "w");
                    if (as.output == NULL)
                    {
                        error(&as, ERR_OWRIT);
                    }
                    else
                    {
                        link(&as, "; Linking %s\n", as.filename);
                        link(&as, ".define _ .\n");
                        link(&as, ".embed \"%s\"\n", as.filename);
                        link(&as, ".define __ .\n");

                        u32 counter = as.counter;
                        as.line_n = 0;
                        as.line_c = 0;
                        as.line_i = NULL;
                        as.pass = 0;

                        as.filename = argv[1];
                        as.input = fopen(as.filename, "r");
                        if (as.input == NULL)
                        {
                            error(&as, ERR_OREAD);
                        }
                        else
                        {
                            execute(&as);
                            if (as.status == AS_OK)
                            {
                                free(as.context);
                                as.context = NULL;
                                as.counter = counter;
                                as.line_n = 0;
                                as.line_c = 0;
                                as.line_i = NULL;
                                as.pass = 1;
                                rewind(as.input);
                                execute(&as);
                            }
                            fclose(as.input);
                        }
                        fclose(as.output);

                        link(&as, ".seek __\n");
                    }
                }

                if (as.linking != NULL)
                    fclose(as.linking);
            }
            else
            {
                error(&as, ERR_FSAME);
            }
        }
        cleanup(&as);
    }
    else
    {
        as.status |= usage();
    }

    return as.status;
}
